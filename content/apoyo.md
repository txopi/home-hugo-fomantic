---
title: "Apoya este proyecto"
date: 2019-10-27T15:54:41+01:00
publishdate: 2019-10-27
lastmod: 2019-10-27
draft: false
tags: []
---



Sindominio se sustenta mediante aportaciones de quienes lo formamos. No hay cuotas, ni precios ni nada que se le parezca, pero puesto que el mantenimiento de la infraestructura física comporta gastos, se espera que quienes participamos en el proyecto nos responsabilicemos también de su financiamiento en la medida que podamos. A modo orientativo, se recomienda una aportación de entre 10 y 30 euros al año para amigas, y de entre 30 y 60 para colectivos. Como decía el colectivo autogestionado La Biblio, "la autogestión tiene un precio" ;)

Nuestro número de cuenta es ES86 3025 0006 21 1433230004 (Caja de ingenieros), si prefieres donar en mano sin transferencia bancaria, ponte en contacto con tu sindominante de confianza.

{{< blockwrap >}}

{{< blockad text="10€" subtext="Nos ayudas a continuar dando servicio"  >}}

{{< blockad text="20€" subtext="Mejoramos nuestro almacenamiento de nube"  >}}

{{< blockad text="30€" subtext="Hacia una máquina propia autoalojada"  >}}

{{< /blockwrap >}}
