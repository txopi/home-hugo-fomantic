---
title: "Herramientas disponibles"
draft: false
tags: []
---

* [Gestión de tu cuenta](#gestión-de-tu-cuenta)
* [Correo electrónico](#correo-electrónico)
    * [Webmail](#webmail)
    * [Thunderbird](#thunderbird)
    * [Claws-mail](#claws-mail)
* [Chat de Matrix](#chat-de-matrix)
    * [Revolt](#revolt)
    * [Riot Android](#riot-android)
* [Chat XMPP/Jabber](#chat-xmpp-jabber)
* [Listas de correo](#listas-de-correo-electrónico)
* [DNS mediante HTTPS](#dns-mediante-https)
* [Sitio web](#sitio-web)
* [Git](#git)
* [Secure Shell](#secure-shell)


¡Bienvenida a Sindominio!

Hay diferentes herramientas y servicios telemáticos de los que puedes disfrutar (como correo electrónico, chat y sitio web) dependiendo si tu cuenta es Sindominante o Amiga, algunos accesibles desde el menú superior.

Las herramientas y servicios de Sindominio también están disponibles a traves de la red Tor que te permite un anonimato casi absoluto.

Aquí te indicamos algunas de las configuraciones que podrás necesitar. 


# Gestión de tu cuenta
<img class="small-block" src="/img/sd-logo-bn-196.png" alt="Sindominio logo">   

Para cambiar tu contraseña y otras funcionalidades que iremos añadiendo:  
{{% bg_blue %}}https://lowry.sindominio.net/{{% /bg_blue %}}  

En la red Tor:  {{% bg_blue %}}http://y6v2daqfsqjihrbzjsrwyghfla25v2cpbp3g2o74te6lfzlu2razh3id.onion{{% /bg_blue %}}

Desde la herramienta web llamada Lowry, puedes realizar algunas operaciones básicas como cambiar tu contraseña o invitar a Amigas (esto último sólo si eres Sindominante).

Recuerda que en Sindominio no damos la posibilidad de recuperación de contraseñas, asi que procura no olvidar tu contraseña, dado que perderías el acceso a tu cuenta!

# Correo electrónico   

Direccion de e-mail: TUNOMBREDEUSUARIA@sindominio.net

Usuaria configuración: TUNOMBREDEUSUARIA

Clave / password: TU CONTRASEÑA ELEGIDA

## Webmail
<img class="small-block" src="img/roundcube.svg" alt="Roundcube logo">   
Puedes acceder con tu navegador al webmail, y conectarte a nuestro Roundcube. 

Webmail:  {{% bg_blue %}}https://webmail.sindominio.net/{{% /bg_blue %}}  

Tor:  {{% bg_blue %}}http://gawsiqs6sulkes4niedkvn6mlcxhqymqrj3jwuceglqworcncyi2alqd.onion{{% /bg_blue %}}   

Además de vía web, también puedes utilizar el cliente de correo de tu elección. Desde Sindominio te recomendamos los que son libres, como por ejemplo Thunderbird, Evolution, K-9 o Claws-Mail.


  
</br>

## Thunderbird  
<img class="small-block" src="img/thunderbird.svg" alt="Thunderbird logo">   

Thunderbird se autoconfigura con sólo rellenar tu dirección de correo de sindominio.net. Pero si no lo consigue:

- **IMAP**: mail.sindominio.net  993 SSL/TLS  
  Tor: asgsory2hji5qpkmgyig2nzquolu5qhsfhz2qaiixdfgivzxlhrahzyd.onion  

- **POP3**: mail.sindominio.net  995 SSL/TLS  
   Tor: asgsory2hji5qpkmgyig2nzquolu5qhsfhz2qaiixdfgivzxlhrahzyd.onion  

- **SMTP**: mail.sindominio.net  587 StartTLS  
  Tor: asgsory2hji5qpkmgyig2nzquolu5qhsfhz2qaiixdfgivzxlhrahzyd.onion  

   (en todos los casos el método de autenticación: Contraseña normal)  

En caso de que no se autoconfigure el tráfico por Tor, asegurate de haber instalado el Add-ons TorBirdy con estos parámetros:  

- Use custom proxy settings:  
  - Socks Host: 127.0.0.1   
  - Port: 9150  

Hacer: test proxy settings  
(para que funcione Thunderbird habrá que abrir simpre Tor Browser)  

</br>

## Claws-mail
Si prefieres configurar tu gestor de mail como Claws-Mail, debes usar otros parámetros. La configuración está en el menú.  

<img class="small-block" src="img/claws-mail.jpg" alt="Claws-mail logo">   

Configuración -> Editar cuentas: selecciona la cuenta adecuada y haz clic en Editar.  

- Pestaña Básicas: 

  - Servidor recepción: mail.sindominio.net  
  - Servidor SMTP: smtp.sindominio.net  
  - Usuaria/Contraseña: los de tu usuaria de Sindominio  

- Pestaña Recibir: 
  - Utilizar autentificación segura (APOP): SIN activar  
  - Pestaña Enviar:  
  - Autentificación SMTP: activada    
  - Método de autentificación: PLAIN  
  - Usuaria/contraseña: dejar en blanco o poner los mismos que en la pestaña Básicas.  
  - Autentificación POP3 antes de enviar: desactivada  

- Pestaña Privacidad:
  - Sistema de privacidad por omisión: ninguno  
  - Pestaña SSL:  
  - POP3: Usar orden STARTTLS para abrir sesión SSL: activado  
  - SMTP: Usar orden STARTTLS para abrir sesión SSL: activado  
  - Usar SSL no-bloqueante: activado  

- Pestaña Avanzadas: 
  - Puerto SMTP: activada y con el valor 587  
  - Puerto POP3: activada y con el valor 110  


</br>

# Chat de Matrix  
<img class="medium-block" src="img/matrix.png" alt="Matrix logo">   
  
Puedes utilizar el chat o servicio de mensajería instantánea y videollamada que Sindominio tiene instalado para escapar de multinacionales y preservar tu intimidad.

Usamos un sistema llamado Matrix que federa y permite hablar con otros servidores, es decir, con usuarias de otros dominios que tengan cuenta de Matrix.  

Acceso vía web con Riot:  {{% bg_blue %}}https://chat.sindominio.net/{{% /bg_blue %}}  

Tor:  {{% bg_blue %}}http://kbn75fbxled3gxe5s6ao6egcwut5uhu4ty5vrdbvhi4x7gvl5b3hfcad.onion{{% /bg_blue %}}  

Usuaria: {{% bg_blue %}}@TUNOMBREDEUSUARIA:sindominio.net{{% /bg_blue %}}  (si defines el servidor no hace falta indicarlo de nuevo aquí por lo que puedes escribir simplemente esto: TUNOMBREDEUSUARIA).  

Contraseña (password): TU CONTRASEÑA ELEGIDA

Aquí podrás participar en diferentes canales, además de tener tus conversaciones privadas:

- **#cafe:sindominio.net**: es una sala pública donde hay mucha gente y se habla de muchas cosas. Muchas de las personas que están allí no tienen relación con Sindominio. 
- **#lacolmena:sindominio.net**: es una sala privada sólo para sindominantes y amigas sindominantes. A esta sala aparecerás automaticamente conectada cuando te conectes con tu cuenta de Sindominio.
- **#asamblea:sindominante.net**: es una sala privada únicamente para sindominantes (y no para las amigas). Este canal sirve para compartir dudas y soluciones, pero no para tomar decisiones o transmitir informaciones relevantes (hay sindominantes en la asamblea que no usan Riot/Matrix). Las decisiones y las informaciones relevantes se mueven y toman en la lista de correo de la asamblea (asamblea@listas.sindominio.net).

En caso de que participes en grupos de trabajo (únicamente para cuentas sindominantes) puede ser que exista un canal propio como las administradoras del server o el grupo de comunicación. No dudes en preguntar a tu madrina sindominante o al grupo de trabajo en el que participas si te surge cualquier duda. Recuerda que son las listas de correo donde se discuten y toman las decisiones. Las salas de Matrix son sólamente el medio utilizado para hablar de problemas o dudas de una forma rápida pero no para transmitir informaciones relevantes ni tomar decisiones.

Puedes instalar clientes de Matrix en tu móvil y en tu ordenador (aplicaciones para leer el chat), como por ejemplo Riot, Revolt, etc. Si en lugar de usar el Riot via web de Sindominio usas otro cliente, deberás introducir también este dato:  
URL del servidor de inicio: https://matrix.sindominio.net  
O bien introducir tu usaria de la forma mencionada: @TUNOMBREDEUSUARIA:sindominio.net  
</br>
Aquí tienes una lista de clientes de Matrix: https://matrix.org/docs/projects/clients-matrix   

</br>

## Revolt 
<img class="small-block" src="img/revolt.png" alt="Revolt logo">   

En GNU/Linux se recomienda Revolt, que tiene capacidad de usar salas cifradas. Otros clientes como nheko o quaternion no tienen esta funcionalidad. Por ejemplo, la sala "asamblea" de Sindominio, sólo accesible para sindominantes, está cifrada. La sala "cafe" de Sindominio en cambio es accesible por cualquier cuenta de Matrix, sea de Sindominio o no. 
En el programa Revolt, para entrar en el chat, al menos la primera vez, hay que hacer lo siguiente:  

- Pulsa el botón "Sign in".  
- En la siguiente pantalla, introduce tu nombre de usuaria de Sindominio, en Username, de esta manera: 
 @TUNOMBREDEUSUARIA:sidnominio.net y tu contraseña.
- Pulsa el botón "Conectar" y si todo ha ido bien estarás dentro, automáticamente te habrá detectado el server matrix.sindoiminio.net sin necesidad de configurarlo en las pantallas anteriores.

Existen bridges (puentes) de Matrix (unos servidores especiales) que te pueden permitir hablar con otras plataformas de chat libres (IRC, Mastodon, Rocket.Chat) y no libres (Telegram, Slack, Discord). Por ejemplo, matrix.org tiene un puente a todo Freenode (probablemente el servicio de IRC con más canales que existe). Gracias a ello, puedes entrar a cualquier canal de IRC alojado en Freenode utilizando tu cliente de Matrix en lugar de utilizar un cliente de IRC. De esta forma, puedes acceder a dos plataformas distintas con un sólo programa. Para acceder al canal de IRC de Sindominio prueba a entrar al siguiente canal de Matrix: **#freenode_#sindominio:matrix.org** Para entrar en otro canal, sustituye "sindominio" por el nombre del canal IRC de Freenode de tu elección.


## Riot Android
<img class="small-block" src="img/riot.svg" alt="Riot logo">   

Puede encontrar esta app en el repositorio F-Droid: https://f-droid.org/es/packages/im.vector.alpha/

Al abrirla por primera vez debes indicar donde queremos conectarte. No debes registrarte, porque ya estas registrada en Sindominio, sino que debes iniciar sesión. Para ello, relena los siguientes campos:

- E-mail o nombre de usuaria: TUNOMBREDEUSUARIA
- Número de teléfono: (no rellenar este campo)
- Usar un servidor personalizado: (activar esta casilla)

Al activar la configuración de un servidor personalizado, aparecen dos campos más:

- URL del servidor de inicio: https://matrix.sindominio.net/
- URL del servidor de identidad: prueba a dejarlo en blanco, aunque si te da problemas escribe: https://vector.im

Pulsa el botón "INICIA SESIÓN" y la app entrará en tu cuenta de Matrix de Sindominio.


</br>

# Chat XMPP (Jabber)  
<img class="small-block" src="img/xmpp.svg" alt="XMPP logo">   

Extensible Messaging and Presence Protocol, más conocido como XMPP (Protocolo extensible de mensajería y comunicación de presencia), se creó en el año 2000 con el nombre Jabber. Varios servicios de chat comerciales se basaron el él. Existen servidores de chat libres (como el de Sindominio), así como cliente de chat libres.
Para usar este servicio de chat libre, federado y que soporta cifrado punto a punto puedes usar Conversations en Android.
Desde tu máquina GNU/Linux puedes usar Gajim o Pidgin, este último también para Windows.  
 
<img class="small-inline" src="img/conversations.png" alt="Conversations logo">
<img class="small-inline" src="img/pidgin.png" alt="pidgin logo">
<img class="small-inline" src="img/gajim.png" alt="gajim logo">

JID:  {{% bg_blue %}}TUNOMBREDEUSUARIA@sindominio.net{{% /bg_blue %}}    
Servidor:  {{% bg_blue %}}sindominio.net{{% /bg_blue %}}  
Tor:  {{% bg_blue %}}vlcucvwysadznjenjt2fvsax2lupzcamfqnstnehqmd4skvnvszma7id.onion{{% /bg_blue %}}  

Más información: https://es.wikipedia.org/wiki/Extensible_Messaging_and_Presence_Protocol


</br>

# Listas de correo electrónico  
<img class="medium-block" src="img/mailman.jpg" alt="Mailman logo">   
Sindominio dispone de un servidor de Mailman mediante el cual se ofrecen distintas listas de correo para grupos y colectivos. Estas son las listas de distribución públicas:
{{% bg_blue %}}https://listas.sindominio.net{{% /bg_blue %}}  

Cada Sindominante puede crear y gestionar las listas de correo que necesite. Todavía no se puede hacer directamente desde lowry, pero más adelante sí.

Es necesario informar a la asamblea por cada lista de correo creada, pero no es necesario esperar la aprobación, basta con explicar brevemente para qué es la lista (puede que le interese a más gente o simplemente puede que alguien conozca una lista muy similar).

Si eres amiga puedes solicitar la creación de una lista a tu madrina sindominante o en esta dirección: sd@sindominio.net.

</br>

# DNS mediante HTTPS
<img class="small-block" src="img/doh.svg" alt="DNS over HTTPS logo">   
Para evitar que las peticiones DNS que hace tu navegador mientras navegas viajen sin cifrar por la red, ahora es posible cifrarlas mediante HTTPS. Sindominio dispone de un servicio DNS mediante HTTPS (también llamado simplemente DoH por sus siglas en inglés) que mejora tu privacidad mientras navegas. Además, este servicio no guarda ningún log de las peticiones DNS recibidas.

Puedes configurarlo en Firefox de la siguiente forma:

- "Preferencias" -> "Configuración de red" -> Activa "DNS mediante HTTPS".
- Seleccionar "Personalizada" y en "Personalizar" pon este valor:
{{% bg_blue %}}https://dns.sindominio.net/{{% /bg_blue %}}

Con esta configuración, Firefox usará el servidor de DoH de Sindominio y en caso de problemas con él, usará la configuración de DNS del sistema.

Es posible configurar Firefox para que sólo use DoH y nunca el DNS clásico, lo que aumenta la privacidad pero puede impedir la navegación si algo falla. 
Si sabes lo que haces y quieres desactivar el DNS tradicional en Firefox, entra en about:config y modifica estos parámetros:  
- **network.trr.mode 3** (para que no haga 'fallback' a DNS clásico)  
- **network.trr.uri https://dns.sindominio.net/**  (para indicar qué servidor DoH quieres usar)  
- **network.trr.bootstrapAddress 88.99.208.38**   (para que sepa cual es la dirección IP de dns.sindominio.net)  

Más información: 

- https://support.mozilla.org/en-US/kb/firefox-dns-over-https
- https://es.wikipedia.org/wiki/DNS_mediante_HTTPS  
- https://es.wikipedia.org/wiki/Sistema_de_nombres_de_dominio



</br>

# Sitio web  
<img class="small-block" src="img/web.svg" alt="Web logo">   

Sindominio dispone de una instalación de WordPress y si quieres un blog, no tienes más que pedirlo en la asamblea (si eres sindominante).  Si eres amiga puedes solicitar la creación de un blog de WordPress a tu madrina sindominante o en esta dirección: sd@sindominio.net.

También puedes crear otros sitios web que sean estáticos ya que no tienen tantos fallos de seguiridad como otros gestores de contenidos más complejos (WordPress, Drupal, etc.). Puedes usar por ejemplo, Hugo (Go), Grav (PHP), Pico (PHP), Jekyll (Ruby) o Gatsby (JS). Te animamos a utilizar sitios web estáticos. Habla con nosotras para activarlo y para que te mostremos cómo funciona.


</br>

# GIT
<img class="small-block" src="img/gitea.png" alt="Gitea logo">   

Sindominio dispone de un servidor Gitea para que puedas crear tus repositorios de código fuente mediante git y colaborar con otras compañeras de Sindominio:   
{{% bg_blue %}}https://git.sindominio.net/{{% /bg_blue %}}  
Más adelante podrás gestionar cómodamente el contenido de tu sitio web estático desde este servicio web.


</br>

# Secure Shell
<img class="small-block" src="img/ssh.png" alt="ssh logo">   

Sindominio dispone de una máquina que da acceso SSH o MOSH a aquellas cuentas que tengan una [shell](https://es.wikipedia.org/wiki/Shell_(inform%C3%A1tica)) activada. Por defecto, las cuentas no tienen shell; tanto amigas como sindominantes deben solicitarlo para que una administradora la active.
El acceso se debe hacer a sh.sindominio.net por el puerto 2222. Escribe lo siguiente en tu terminal:  

> ssh -p 2222 tucuenta@sh.sindominio.net

En esta máquina, una vez hayas accedido mediante SSH o MOSH puedes ejecutar programas como por ejemplo el cliente de IRC IRSSI, subir ficheros en tu home (mediante scp) o usarlo de pasarela para salir por otra IP, creando lo que se denomina un tunel SSH.

Si en tu home creas una carpeta llamada "public_html" y dejas ficheros, como por ejemplo, fotos o textos, serán automáticamente accesibles mediante el navegador en: https://sindominio.net/tucuenta. Por tanto, esto te permite compartir rápidamente una serie de ficheros de forma pública.


</br>
</br>
</br>
{{% bg_blue %}}Si tienes dudas [contáctanos](/contacto/),{{% /bg_blue %}}
{{% bg_blue %}}¡Nos vemos por la red de Sindominio!{{% /bg_blue %}}


