---
title: "No se puede decir que diez años no son nada. (2009)"
---
*Texto del compañero Roxu enviado a la Asamblea de Sindominio, con motivo del 10º aniversario del proyecto.*
<img src="../../img/sd-banner.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

Lunes 19 de octubre de 2009.

El tango hablaba de 20 años pero creo que con diez ya nos podíamos dar con un canto en los dientes. Y es que nunca pensamos en perdurar, ni mucho menos, sino mas bien simplemente luchábamos por existir; tarea nada sencilla si tenemos en cuenta que en un momento como aquel era difícil saber con cierta concreción que es lo que queríamos hacer. Así pues, cuando hace exactamente diez años [1] Alejo, Joseba y Miquel conectamos a Internet a Fanelli, y mientras esperábamos la respuesta positiva de Marga, que estaba al otro lado para comprobar que todo funcionaba correctamente, el proyecto SinDominio pasó a ser una verdadera realidad en la Red.

Hasta entonces todo habían sido listas de correo (aquella abierta por los compañeros de la ECN italiana), paginas con listados de adhesiones (albergada en TAO, servidor canadiense con el que Quique tenía contacto), reflexiones acerca de la necesidad de gestionar y autogestionar recursos tecnológicos -mas adelante, fundamentalmente a lo largo de los primeros meses, ya veríamos como se realizaría esa gestión horizontal de la que tango hablábamos-, conflictos y muchas muchas mas alianzas, que provenían de espacios e iniciativas con alguna experiencia en ‘lo digital’ o por el contrario de aquellas otras que sin tener ni la mas remota idea de lo que podría ser «un servidor autogestionado en Internet» se lanzaron a participar en esta red con la ilusión de buscar nuevas formas de entender la política o, porque no, otras vías de comunicar; no olvidemos que para SinDominio supuso un gran espaldarazo el interés de la red de colectivos de contrainformación Contrainfos.

Claro está el componente tecnológico era primordial, se acercaba un avance en las comunicaciones humanas nunca visto en tiempos y aunque lo percibíamos de forma ambivalente, entre el mercado y las posibilidades de transformación, teníamos la certeza política de lo trascendental de todos estos cambios en el Mundo. Pero como ya tantas veces se ha relatado, la carestía técnica en todos nosotros era notable y en aquellos momentos, con una incipiente comunidad del software libre, no era fácil encontrar como adquirir los conocimientos para montar los sistemas necesarios para albergar una iniciativa compleja e innovadora como era SinDominio. Hay que tener en cuenta además que muchos de los deseos de horizontalidad, de ruptura entre clientes y administradores, eran inviables porque no sabíamos como plantear la solución técnica apropiada para cada necesidad. Con el tiempo esto fue cambiando y aunque muchas ideas se quedaron por el camino, otras muchas si funcionaron.

Con todo esto SinDominio salió adelante y ha llegado a los 10 años. Me alegro de ello y estoy seguro de no ser el único. Aunque sea de forma individual y aislada os invito a celebrarlo de la manera que os plazca -personalmente yo optaré por un clásico gintonic. No obstante, recordar y celebrarlo no basta y tengo la sensación de que en estos momentos debemos plantearnos hacer algo mas.

A día de hoy creo que no existe nadie en SinDominio que no eche de menos algo de iniciativa colectiva, de búsqueda de un sentido común a todo esto, mas allá de la continuidad obligada por los servicios que se prestan a propios y extraños. Muy probablemente algunos debates no fueron superados y estamos lastrados por no pocos conflictos. Sin embargo si no hemos decidido apagar las máquinas será porque algún hilo de mínimo interés aún perdura. Ya digo, nunca nos propusimos un plazo, ni un objetivo y si seguimos aquí ha de ser por una verdadera voluntad compartida y no por la atadura de estar aquí para siempre.

Por suerte no son pocos los retos de hoy en día y precisamente por ese motivo quizá se mas fácil encontrar las energías suficientes para generar un espacio de pensamiento capaz de hacer evolucionar SinDominio o, si fuese necesario, poner en marcha otra nueva iniciativa o, espero que no, dejarlo todo como está.

¿A alguien le interesa seguir hablando?

un*rx

[1] Éste ‘exactamente’ es una pequeña licencia, ya que todos sabemos que el momento de la conexión de Fanelli a Internet no fue hoy, ni ayer, ni mañana hace dies años, fue el día 9 de octubre de 1999, mas o menos a eso de las 12:00h. Hicimos fotos, pero no sé si estarán por algún lado, eso si, en el lateral de la CPU Aljshop inmortalizó el momento poniendo una pegatina que se movía por Madrid en aquellos tiempos, sólo decía «Autonom@s».

*Editado minimamente por Martintxo.*


[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)

