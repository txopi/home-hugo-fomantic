---
title: "sinDominio.net: telematika antagonistaren eta adimen kolektiboaren aldeko erronka"
---
<img src="../../img/sd_zona_temporalmente_autonoma.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

2003ko urriaren 1a, asteazkena.

Jakin badakigu, orain dela urte batzuk aberasteko grinarik gabeko zerbitzari telematiko batzuk daudela Espainiar estatuan -Ipanex-en federatuak [* Berria: orain ez dago martxan]- ezker sozio-politikoan dauden kolektibo, elkarte eta gizabanakoei Internet konexioak eskaintzen zituztenak zerbitzari komertzial edota instituzional batekin lotuta egon gabe, beren edukinak argitaratu ahal izateko. Arlo honetan lan handia egin dute, Interneterako sarbidea errazten , eta hainbat kolektibo zein elkarte argira ateratzen. Halere, esan behar da GKE-ak izan direla nagusi eremu honetan. Konexioa lortzearen aldeko erronka aukera bat da, baina ez da aukera bakarra. Horrez gain, batzuok pentsatzen dugu konexio askearen aldeko aldarrikapena eta "guztiontzako sarbidea" garrantzia galtzen ari direla kapitalaren interes estrategikoa hedatzen den heinean. Kapitalak bere zerbitzuak sarearen bidez egin ahala, jendea konekatuta egotea ezinbesteko bihurtzen da. Jakina, Iparralde aberatsari buruz ari gara. "Hirugarren mundua" deituriko horretan egoera oso bestelakoa baita (5 pertsonetik 1k telefonoa eskura dauka), baina jarrera neoliberala, hau da, "guztiontzako sarbidea" globala eta geldiezina da.

### Edukinak konektibitatearen aurka? Politika tutueriaren aurka? Adimen kolektiboaren alde

Horrek ez du baztertzen denontzako sarbide askea (esate baterako, gaztetxe edota gune autogestionatuetatik), baina horrekin ez dugu esan nahi "eremu materialerako sarbidea" -konexio teknikoa, hain zuzen-, gero eta merkeagoa eta orokorragoa dena. Ez dugu bestaldetik esan nahi "edukinetarako sarbidea" (informazio-kontsumoa edota mundu mailan hedatzen diren edukinen kontsumoa). Esan nahi duguna zera da: adimen kolektiboetarako prozesu guztien sarbidea. Ziberespazioa gizabanakoen adierazpen sistema zabala eta kooperatiboa bezala ulertuta, hau da, erabakiak hartzeko eta elkar-ikasketa eta jakinduriaren zirkulazio askea egoteko gunea.

Izan ere, ziberespazioak daukan ahalmena edukin alternatibo edota kritikoak argiratatzeko gunea baino askoz zabalagoa denez, sareak, egitasmoak, borrokak, nahiak eta errealitateak elkarkonektatzeko bidea ematen digu. Ziberespazioa ez da ixilpean gordeta dauden mezuak ateratzeko bide bat baino ez. Eremu honek bestelako praktika politikoak gauzatzeko balio du –beste modutan modu eskasean eta lokalean geratzen direnak-.

Esan behar da –berriro ere errepikatzeari ekingo badiogu ere-, ziberespazioa ez dela tresna bat, ez dela azpiegitura bat, baizik eta azpiegiturak erabiltzeko eta pertsonak konektatzeko bide egoki bat, benetako giza-mugimendu bat, estatuetatik eta multinazionaletik at modu kooperatibo batez gauzatu dena.

Honekin ez dugu inolaz ere esan nahi jendeak utzi behar dituela bestelako borrokak eta ziberespazio honetan murgildu behar duela (mundu batek ez du zertan bestea mugatu behar), baina aldi berean, ziberespazioa erabil daiteke lurraldean gu geu hobeto kokatzeko, gizarte autodeterminatu bat eraikitzeko, elkar komunikatzeko, estatu eta instituzionetik kanpo. Ziberespazioak balio izan behar izango du eztabaidak, kanpainak eta elkarrekiko ekintzat bultzatzeko, aniztasuna bultzatzeko eta kooperazio eta elkar-entzuketarako era berriak eta orain arte ezagutu ez ditugun demokraziarako modu berriak eratzeko.

### Zer nahi du Sindominiok?

Sindominiok sarean zehar autoorganizatzen eta mugitzen den multiverso horretan murgildu nahi du, orain dispertsaturik edo saretik kanpo aurkitzen diren errealitatre antagonistak potentziatu eta erakutsi, eta ahal duena gehitu kooperazio eta komunikazio, eta gatazaka eta borroka, gune horretan, non izugarrizko proiektu autogestionatuak dauden, adibidez GNU/Linux sistema operatiboa sortu duena, hau da, ziberespazioan zehar dagoen eraikuntza kolektibo adibiderik nagusiena –Internet sare berarekin batera-. Tresna libreekin frogatu eta kooperatu, ikertu eta software-aren arlo estrategikoan komunitate birtualekin ezagutzak elkartrukatzeko makina Linux bat behar dugu.

Sindominiok gure proiektuaren antzerakoak diren beste proiektu batzuekin koordinatu eta koopertu nahi du baita ere, adibidez ECN italiarra, non proiektu telematikoaren eta bertan koordinatzen diren errealitateen arteko harremana eta inplikazioa erabatekoa den eta non zerbitzu, tarifa,etab.ean beste zerbitzari batzuekin konpetizea gainezko kontu bat den ez dutelako konektibitaterik eskeintzen: Sindominion parte hartzen duena, bezero batean edo diruaren truke zerbitzu bat eskeini behar zaion erabiltzaile huts batean ez dadila bilakatu behar dugu. Sindominio proiektu militante bat da eta aportazio ekonimikoz mantendu behar da, soldatadun, liberatu edo denbora guztia dedikatu behar dioten pertsonik gabe. Horrek kultura desberdin bat eratzera behartzen du, pasibitatea alde batera utziz ordenagailuen erabileran,beste arlo batzuetan kriterio alternatiboekin mugitzen den jendeak, "zerbitzuak ematea"ren ideiarekin apurtzea, Interneten dauden erabilera arrunt, komertzial eta despolitizatuen aurrean kritikoki kokatzeko zentzuzko elementuez ornitzea, eta komunitate birtual eta inteligentzia kolektiboarekiko apustuak dituen gauza on guztiak ikastea.

Baina batez ere Sindominio mugimendu antagonista bezala erabiltzen bada da soilik bideragarri eta interesgarri. Hau da, komunikazio alternatiborako tresna bezala eta autogestioarengatik borrokatzen duten eta hesparru desberdinetan auotonomia soziala bultzatzen duten pertsona eta kolektiboen koordinazio eta kooperazio bezala. Zentzu honetan, Sindominiok oinarri materiala eskeiniko dio UPAren etengabeko eraikuntzan dagoen agentzia bezalako proiektuari adibidez, eta baita contr@infos-i [* Berria: orain bi proiektu hauek ez daude martxan] edo dokumentazio antagonista guneari, gainera posta zerrendak, e-mail eta web espazioak ere eskeiniko dizkio Sindominio proiektuan inplikatu nahi duten errealitate antagonista guztiei (hau da, autogestionatuak eta oinarrikoak, ez instituzionalak ezta partidariak).

### Zertan datza Sindominio?

Sindominio proiektuaren ideia da makina bat Interneti konektaturik edukitzea 24 orduak, sareen sareari sarrera duen munduko edozein lekutatik ikusgarria izanik. Makinak dominio bat -sindominio.net- edukiko du. Dominio bat Interneten dagoen edozein makinak munduko edozein lekutatik aurkitu dezakeen izen baten bidez errepresentaturiko helbide fijo bat da, adibidez posta konkretu bat bidaltzeko, posta elektronikoaren helbidean arrobaren eskubikaldean dagoen zatia da dominioaren izena.

Gure Linux makinaren gain honako zerbitzu hauek funtzionatuko dute: webgune zerbitzaria, posta elektronikoko zerbitzaria, posta-zerrendak, news (debate taldeak), bilatzaileak, artxibo eta dokumentazio gunea, contr@infos-en berri agentzia [* Berria: orain ez dago martxan], mirrors (hau da, bahiketetatik babesten duten eta sarean trafikoa leuntzen duten zerbitzari kopiak)... Gure ezagutza baino muga gehiagorik ez dago, gure irudimena eta baliabide hoiek aprobetxatzeko gogoak baino muga gehiagorik ez dago: Sindominion parte hartzen duenak erabateko gertutasuna edukiko du makinarengan, hau da, ez da egongo edukin eta zerbitzuen arteko zatiketa merkatal zorrotz tipikoa (horrela laburtzen dutelarik: "eskegi zure informazioak, guk falturatuko dizkizugula zerbitzuak").

Linux-ek erabateko urruneko administrazioa onartzen du eta makinaren kokapen fisikoa guztiz indiferente bilakatzen du, momentu berean leku desbedinetatik administratua eta gestionatua izan daitekelarik.

### Nola parte hartu proiektuan?

Madrilen ospatutako Kontrainformazio topaketetan (http://www.nodo50.org/contrainfos), Penintsulako puntu desberdinetatik etorritako kolektibo batzuek proiektu goizale hau burutzea erabaki genuen. Bere aldetik, ECNko kideak gure eskura jarri dute koordinatzeko erabiltzen dugun posta zerrenda bat [* Berria: orain gure makinan dago]. Hobeto informatu edo bertan parte hartu nahi baduzu, idatziguzu sd@sindominio.net-era.

Sindominiori laguntzeko era da urtero 30 euroetako aportazioa egitea, baina hasteko ahal duenak gehiago emanez gero hobeto, lehengo momentuan makinari eman nahi zaion erabileraz aparte. Diru hau bi gastu ordaintzeko balioko du: makinarena, eta emandako lerro batetako ornitzaileari (Internetera sarrera eta irteera) eta makina ipintzeko gune fisikoaren (housing) urteko alokairua.

**Kontu korrontea: 3025 0006 21 1433230004 (Caja de Ingenieros).**

[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)

