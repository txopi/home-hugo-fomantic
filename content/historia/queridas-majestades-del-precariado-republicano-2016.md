---
title: "Queridas majestades del precariado republicano. (2016)"
---
<img src="../../img/sd_logo.png" alt="sindominio.net" style="width:25%; text-align: center;"/>


05 de enero de 2016, en un lugar cuántico del Estado español

Queridas majestades del precariado republicano:

En el inicio del nuevo año y del nuevo ciclo biopolítico, la asamblea de sindominantes -esa comunidad de personas y colectivos defensores de las tecnologías/comunicaciones libres/seguras y de los usos sociales de las mismas- envía este comunicado sideral para decir que NO CERRAMOS y que, además, hemos conseguido recaudar los 5.000 euros necesarios para sostenernos durante este transformador 2016, gracias a generosos botes de apoyo y donaciones de colectivos y activistas.

La realidad es que con nuestros planes de cambios -planteados de forma somera en nuestra última asamblea presencial de 2015 (Traficantres de sueños, Madrid, 4 de octubre)- traemos una serie de deseos y peticiones que les hacemos a ustedes, vuestras majestades intersiderales. Vamos a ver lo que conseguimos.

1) El proyecto tecnopolítico

Sindominio (1999-2015) Y ahora, ¿hacia dónde?

Queremos concretar contigo y con la otra, y con la del quinto, y con la del pueblo, el sentido político de Sindominio, una comunidad formada no solo por técnicos y hackers, sino por activistas y cualquier persona interesada en el desarrollo de las comunicaciones libres y seguras. Sindominio se diferencia de otros servidores en que no ofrece un servicio en plan proveedor/cliente. Sindominio es un servidor autogestionado, con los límites y potencialidades que ello implica.

¿Y qué podemos hacer desde el ámbito tecno-comunicatico? Sus majestades, nuestra petición es que nos deis el coraje, el amor, el entusiasmo y la creatividad necesarias para apostar por un nuevo Sindominio que esté a la altura de los tiempos. Esto se concreta en dos grandes deseos, que quizás sean compartidos por ustedes:

A) Una Internet de todas, neutral, organizada por sus habitantes y no por el interés económico. (Estamos abiertas a propuestas).
B) Unas habitantes de las tecnologías empoderadas y autónomas, que han dejado de ser consumidoras y se han convertido en partícipes.

2) Las máquinas

Este es un proyecto que reinventa los lugares/tiempos para la transformación desde los distintos territorios del Estado español. Nos gustaría volver a articular redes y alianzas globales, más que necesarias en un presente de emergencia social y cultural.

En la actualidad, albergamos cientos de listas de colectivos, así como correos electrónicos y mensajería instantánea de individualidades que quieren tener unas comunicaciones seguras (no controladas por multinacionales como Google y Facebook). Alojamos a centenares de páginas web, entre blogs, foros y wikis. Una miríada de habitaciones propias y comunes.

Sus majestades, ahora mismo estamos en un tránsito de alojamiento y de servidores, por ello todavía estamos definiendo qué máquinas y qué lugares físicos necesitamos… ¿Nos regaláis una máquina? ¿Nos regaláis una Zona Temporalmente Autónoma? Nuestra isla tortuga se la comieron los mercados de (infra)valores.

3) La situación económica

Ya hemos dicho que nuestras necesidades básicas para este año que empieza las tenemos cubiertas. Pero, ¿y el futuro?, ¿y la sosteniblidad? El porvenir es un presente continuo… Así que cualquier aportación seguirá siendo bienvenida (y necesaria), cualquier aportación material e inmaterial.

Vuestras majestades, nos gustaría retomar el espíritu de la política partisana, ya saben ustedes, tan sabias… Una tecnopolítica de la gente, del presente, de la amistad, arraigada en el terreno de las redes sociales y los servidores autónomos, con el apoyo mutuo y la solidaridad como valores principales. ¿Y qué haremos en esta crisis también de valores? Lo que hagamos lo haremos pasito a pasito, sin los agobios ni el estrés propios de brokers y otras ludópatas. Nos molan las slow tecnologies, lo que podríamos llamar “tecnologías decrecentistas”. Somos raritas, sí, como la mayoría social, y por eso nos encanta la “baja disponibilidad”. Así que, vuestras majestades, toda nuestra disponibilidad para que estos deseos se hagan realidad.

Las entusiastas sindominiantes

 

PD: “La idea del proyecto Sindominio es tener una máquina conectada las 24 horas del día a Internet, visible por tanto desde cualquier lugar del mundo con acceso a la red de redes. La máquina alberga un dominio virtual: sindominio.net. Un dominio es una dirección fija en Internet representada por un nombre que cualquier máquina desde cualquier parte del mundo puede localizar, por ejemplo para enviar un correo electrónico o para solicitar ver el contenido de una página web. Esa máquina tendrá instalado GNU/Linux, un sistema operativo libre desarrollado de manera cooperativa y no mercantil por miles de programadores y usuarios que lo ponen a prueba, lo traducen a diferentes lenguas, escriben ficheros de ayuda o desarrollan “paquetes” (Debian) para facilitar su instalación. El sistema operativo es fundamental (sin él un ordenador es como una caja de zapatos, no puede ni arrancar), porque de él depende que se puedan usar herramientas libres o no. Sobre nuestro Linux, dará una serie de servicios: servidor de páginas web, servidor de correo electrónico, de listas de correo, de noticias, buscadores, agencia de noticias, mirrors… No hay más límite que el de nuestro conocimiento, que el de nuestra imaginación y ganas de aprovechar esos recursos: quien participe en sindominio tendrá plena disponibilidad de la máquina”.

IV Encuentro Estatal de Contrainformación. Madrid, abril de 1999


[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)

