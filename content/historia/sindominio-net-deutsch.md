---
title: "sinDominio.net: häufig gestellte Fragen"
---
<img src="../../img/sd_zona_temporalmente_autonoma.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

Jueves 25 de diciembre de 2003.

### Warum gibt es sindominio.net?

Gegenwärtig ist klar (und es wird immer klarer) da das Internet ein groartiges Kommunikationsmittel ist: es erlaubt eine sichere Kommunikation (ohne Zweifel sicherer als jede andere), die schnell und vielseitig ist (man kann Nachrichten jeder Art verschicken: Texte, Bilder, Audios, Videos…). Grundsätzlich machen all diese Eigenschaften das Internet zu etwas, das wir alternativen Kollektive nutzen MÜSSEN.

Aber Internet zeigt bei der alltäglichen Nutzung einen Haken, der in vielen Fällen nur auf politischer Ebene besteht, stellenweise jedoch auch auf praktischer Ebene: wir hängen völlig von UNTERNEHMEN ab, die uns die Verbindung, die Mail-Boxen, die Web oder FTP Stellen zur Verfügung stellen… entweder aus direktem (die Dienste müssen bezahlt werden) oder indirektem Interesse (Einbeziehung von Werbung, Kundenaquise für andere). Auerdem sind wir für diese Unternehmen niemals mehr als KUNDEN, die für einen Dienst zahlen, wodurch wir von jeder Entscheidung ausgeschlossen bleiben, von jeder Möglichkeit, die über die beanspruchten Dienste hinausgeht,…

Angesichts dieser Situation hielten Leute aus Madrid und Barcelona die Kreation eines FREIEN Orts im Internet für notwendig, in dem es weder UNTERNEHMERINNEN noch KUNDEN gibt, wo die Möglichkeiten der Nutzung des Mediums Internet nicht durch die Bezahlung beschränkt wird, wo sich alle an der Entscheidungsfindung beteiligen. Dafür war es notwendig, zu organisieren, zu veröffentlichen und einen Computer 24 Stunden täglich ans Internet anzuschliessen. Dies war – und ist – der Ausgangspunkt von Sindominio.

### Geschichte von Sindominio.net

Das erste Mal, da davon gesprochen wurde, einen Server öffentlich und in einem Forum der Bewegung aufzuziehen, war auf einer Tagung über neue Perspektiven der sozialen Zentren, Ende September 1998. Bei dieser Gelegenheit wurde in verschiedenen Sitzungen über die neuen Technologien gesprochen, Telematik usw. An dieser Tagung nahmen Leute aus verschiedenen, besetzten und unabhängig geführten sozialen Zentren Barcelonas und Madrids teil, und man kam überein, die Idee zu verbreiten und das Projekt voranzutreiben.

Etwas später hatten sich bereits 25 Kollektive und soziale Zentren aus dem gesamten Staat zusammengefunden. Auf dem vierten Kontrainformationstreffen (http://www.nodo50.org/contrainfos/) bekam das Projekt sogar noch mehr Schwung, mit Unterstützung des gesamten Kontrainformationsnetzes im spanischen Staat. Zu dieser Zeit standen wir immer noch ohne Maschine da, ohne registrierte Domain, ohne IDP, der es uns erlaubt hätte, eine Maschine ans Netz zu schlieen und ohne Geld. Dazu kam, da unsere Erfahrungen mit den technischen Seiten ziemlich dürftig waren. Wir mußten uns daher ziemlich viel Mühe geben, unser Ziel zu erreichen.

“Isole Nella Rete” (http://www.ecn.org) war so nett, eine Maillist für uns aufzunehmen, dank der wir uns koordienieren und eine permanente Versammlung aufbauen konnten, an der alle teilnahmen, und, noch wichtiger, in der über alles gesprochen wurde. Es stand für uns auer Frage, da die technische Seite nicht von der politischen getrennt werden durfte; – auch wenn es für einige hart sein mag, Gesprächen über TCP/IP oder Debian zuzuhören, glauben wir, da es fundamental war, einen Ort aufzubauen, um das Wissen zu sozialisieren, und wir denken, da eine Art, dies zu tun, unsere Versammlung war.

TAO (The Anarchy Organization, http://www.tao.ca) erwies uns einen groen Gefallen damit, uns ihren Nameserver (DNS) anzubieten um unsere Domain registrieren zu lassen und vorübergehend unsere Seiten auf ihrem Server unterzubringen.

Später, im Juli 1999, wurde die Maschine gekauft: ein Pentium II mit 450 MHz, 256 Mb RAM und 9 Gb Festplatte, die wir ?Fanelli? genannt haben (nach einem Anarchisten, der auf der iberischen Halbinsel die Erste Internationale verbreitete). Bei einem Treffen in Madrid wurde eine gemeinsame Installation vorgenommen, zu der alle mit ihrem Einsatz beigetragen haben und vor allen Dingen auch viel darüber gelernt haben, was ein Server ist und wie die verschiedenen Abteilungen funktionieren. Jetzt fehlte nur noch ein ISP und, wie immer, etwas Geld. Da die Konfiguration der Maschine nicht ohne Mitarbeit aller betroffener Personen stattfinden konnte, dachten wir daran, die Maschine über PPP einige Stunden am Tag anzuschlieen, auch wenn das nur dazu dienen würde, einige Proben zu machen.

Nachdem wir die Preise verschiedener ISP durchgesprochen haben, erschien das Angebot des ISP Kender in Bilbao am interessantesten, mit dem ein Freund von uns auerdem im persönlichen Kontakt stand. Das Problem war allerdings, da sich der IPS im August nicht um uns kümmern konnte, so da wir diese Zeit damit verbracht haben, die Maschine zu konfigurieren und darüber zu diskutieren, ob ein Mailserver besser ist als ein anderer, etc.

Endlich, im Oktober 1999, feierten wir das groe Ereignis: wir waren nicht mehr nur ein einfaches Projekt, sondern Realität: unser Maschine war endlich angeschlossen und betriebsbereit. Es war ein schwieriger und dornenreicher Weg bis dahin, aber Dank des allgemeinen Einsatzes und der erhaltenen Unterstützung (sowohl von der Bewegung im spanischen Staat, als auch von bereits bestehenden internationalen Projekten wie INR und TAO) haben wir es geschafft.

Der gröte Teil der Dienste ist bereit voll einsatzfähig: Mails, Web-Server, FTP, SSH, Maillists….

Eines der innovativsten Projekte ist die “Agentur in permanenter Konstruktion” (“Agencia en Construcción Permanente”), ein Nachrichtenserver in Echtzeit, mit der Besonderheit, da man nicht nur Nachrichten anbringen und lesen, sondern diese auch kommentieren, erweitern, diskutieren kann… Die Agentur ist noch in der Testphase, aber bereits im Einsatz. Wir setzen groe Erwartungen in dieses Werkzeug und laden alle dazu ein, die Agentur zu besuchen und zu nutzen. Sie ist zu finden unter http://acp.sindominio.net

### Ziele

Sindominio entstand nicht mit der Idee, ein anderes Medium für einen Internetanschluß zu sein (tatsächlich bietet SD keinen Anschlu an, es gibt also keine Telefonnummer von Sindominio, die man anruft, um sich anzuschlieen), sondern ein Bezugspunkt für politische Aktivitäten sowie für Internet und freie Software (Bereiche, die bisher wenig bearbeitet wurden) – vor allen Dingen jedoch für die alltäglichen Kämpfe, die jedes Kollektiv oder jede Person auf der Strae austrägt. Sindominio will nicht einfach nur den alternativen Kollektiven in der hispanischen Welt Sichtbarkeit (Websites) verschaffen, sondern strebt an, ein INSTRUMENT DER KOLLEKTIVEN INTELLIGENZ zu sein, wie es verschiedene Teilnehmer des Projekts zu sagen pflegen, ein neuer Bezugsrahmen, um Erfahrungen zu teilen, neue Projekte zu machen usw.. Kurz gesagt, um uns zu verständigen, was enorm wichtig ist, vor allem, wenn es direkt geschieht.

Mit SD wollen wir auerdem den Cyberspace als einen weiteren Raum für unsere Kämpfe zu potenzieren. Wir glauben, da in der ?alternativen? hispanischsprachigen Welt die Nutzung der Cyberspace Werkzeuge noch viel zu begrenzt und selten geschieht. Das liegt häufig daran, daß Wissen über das Thema fehtl, oder weil nicht gesehen wird, da der “virtuelle Kampf” Rückwirkungen auf den “echten” hat. Ein anderes unserer Ziele ist daher, die Nutzung der Kommunikationshardware und ihrer Abkömmlinge zu fördern und auszuweiten (natürlich ohne zu übertreiben) und das Wissen darüber zu sozialisieren.

Wir glauben fest an die Philosophie der freien Software, – so läuft unsere Hardware unter dem Betriebssystem Debian GNU/Linux und fast alle unserer installierten Programme stehen unter allgemein öffentlicher Lizenz GNU (GNU/GPL General Public License). Die Gemeinschaft der “Free Software” besteht aus Menschen auf der ganzen Welt die, völlig ohne finanzielle Interessen Programme von und für den Rest der Welt (weiter)entwickeln. Wir glauben, da dies wahrscheinlich eines der besten Beispiele auf globaler Ebene ist, da die Welt sich anders drehen kann und unser Ziel ist u.a., dies weiter bekannt zu machen.

### Funktionsweise

Sindominio besteht aus und für Kollektive: wir werden all das machen, was wir tun wollen und können und was wir mit unserer Arbeit voran treiben wollen (wir werden nie mit Antworten des Typs “diesen Dienst bieten wir nicht an” oder “das kostet X Geld mehr” konfrontiert werden; die einzigen Grenzen werden technischer Art sein: die uns zur Verfügung stehende Computerkapazität ” die ausreichend scheint ” und unser Wissen ? was man immer erweitert werde kann). Es gibt keine Angestellten und es wird sie auch in Zukunft nicht geben, es muß also die Arbeit der beteiligten Leute sein, die das ganze Projekt voran treibt (tatsächlich funktioniert es bisher bereits so).

So funktioniert Sindominio also horizontal und dezentralisiert: wir koordinieren uns über eine Maillist, in der absolut alle Kollektive, Individuen und Realitäten, die an SinDominio teilnehmen, aufgeführt werden. Wenn jemand zu etwas Stellung nehmen möchte, scheibt er/sie es auf und schickt es an die Liste. Wer antworten will, antwortet. So wurde eine Art “virtuelle Versammlung” geschaffen, an der alle gleichermaen teilnehmen können. Diese Erfahrung ist bereits an und für sich befriedigend. Seit kurzem haben wir außerdem noch eine Verwaltungsliste ins Leben gerufen, in der eher “bürokratische” und/oder “technische” Themen des Projektes besprochen werden.

Diese Liste wurde erstellt, um den Umfang der Versammlungsliste dadurch etwas abzuspecken, daß beinah unverständliche Fragen (z.B. technische Probleme) und mehr oder weniger mechanische Aufgaben, die sich wiederholen und die zuviel “Lärm” in den Mailboxen verursachen aus dem Weg geschafft werden. Die Liste steht jedem Beteiligten von Sindominio zu jeder Zeit offen. Technisches Wissen ist nicht notwendig, es reicht, Lust zum Lernen zu haben. Wir, die wir momentan die “technische Seite” des Projekts tragen, bemühen uns darum, es denjenigen einfach zu machen, die sich das notwendige Wissen aneignen wollen, um auch an der technischen Seite mitzuarbeiten. Dafür werden wir versuchen, für einfachen Zugang zu Anleitungen zu sorgen, “Tagebuch” zu führen über das, was wir tun (warum und wofür), Bücher zu empfehlen…

Es gibt auerdem noch andere Bereiche, die auch über die Liste laufen: Übersetzungen, Beziehungen zu anderen Projekten usw. Wir ermuntern alle TeilnehmerInnen von SD, sofern sie ein wenig Zeit haben, sich auch an dieser Liste zu beteiligen; wir alle können etwas zum Ganzen beitragen.

Schlielich gibt es noch eine “Kaffetenliste”, wo über Angelegenheiten gequatscht wird, die keinen direkten Bezug zu Sindominio haben (off-topics). In dieser Liste ist jedes Thema Willkommen, hauptsächlich geht es um Probleme mit unseren Computern zu Hause und freie Software.

### Wie kann ich teilnehmen

Um bei SD mitzumachen, IST ES NICHT NOTWENDIG etwas von Computern zu verstehen. Sindominion ist ein POLITISCHES PROJEKT, kein Informatikprojekt. Es soll versucht werden, es Kollektiven und Individuuen zu ermöglichen, das Internet auf eine FREIE und TEILNEHMENDE Art und Weise zu nutzen. Deshalb reicht es aus, von einer anderen Kommunikationsform ” auch im Internet ” überzeugt zu sein und auf positive Weise am Projekt teilzunehmen, bereit, es wachsen zu lassen, neue Möglichkeiten zu suchen, ihm einen Nutzen zu geben…

Wenn die Idee interessiert und ihr mitmachen wollt, schickt uns eine Mail, in der ihr euch kurz vorstellt indem ihr erklärt, wer ihr seid, was ihr macht, was ihr von sindomino erwartet und was ihr meint, beitragen zu können, etc. Das ist kein Test, sondern wir möchten uns einfach ein wenig kennenlernen, da wir ja auch zusammenarbeiten werden. Die Anträge werden in der Versammlung vorgestellt, um zu sehen, ob sie Unterstützung erhalten oder ob es jemanden gibt, der dagegen ist, und nach einer Woche wird die Entscheidung getroffen.

Wenn ihr einen öffentlichen Schlüssel für PGP oder GnuPG habt, schickt ihn uns auch, wir verwenden ihn dann, um euch das verschlüsselte Kennwort zu schicken und fügen ihn zum Schüsselbund von sindominio.

Wir erlauben uns, noch einmal darauf zu bestehen, daß Sindominio nicht einfach Websites oder Mailadressen für andere Gruppen bereitstellt. Ihr werdet ein Teil von Sindominio sein, und als ein solcher habt ihr Zugriff auf alle existierenden oder noch zu schaffenden Dienste. Wir hoffen und erwarten, daß alle Teilnehmer sich in das Projekt einbringen und in den Versammlungen aktiv sind, natürlich immer innerhalb der eigenen Möglichkeiten.

### Finanzierung

Bleibt zum Schlu die finanzielle Frage. Sindiominio verkauft keine Services, Sindominio sind wir alle: gemeinsam zahlen wir die Kosten, die der Unterhalt des Servers mit sich bringt und die auf ungefähr 400.000 Peseten jährlich kalkuliert werden (ungefähr 2500 Dollar).

Als ORIENTIERUNGSSUMME schlagen wir 30 euro pro Kollekitv jährlich vor. Es hängt jedoch von den Möglichkeiten jeder Gruppe ab: es gibt Gruppen, die wesentlich mehr beigesteuert haben, während es andere gibt, die aufgrund ihrer finanziellen Situation keinen Pfennig zusteuern konnten. Wir setzen darauf, die finanziellen Schranken zu überwinden und alle gleichermassen teilnehmen zu lassen, unabhängig vom Geld, das ihr Sindominio zukommen lassen könnt.

**Unser Bankkonto ist: 3025 0006 21 1433230004 (Caja de Ingenieros).**

[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)

