---
title: "A por dos decadas libres de dominio. (2018)"
---
<!-- img src="../../img/sd-banner.jpg" alt="sindominio.net por un dominio antagonista en la red" style="width:100%;"/ -->

### Donde estamos y de donde venimos

Cuentan que, allá por 1999, internet era un lugar muy diferente. Todavía no había sido asediado por las corporaciones. Había mucho espacio para experimentar y construir cosas nuevas. Los movimientos sociales lo percibían como herramienta para usar, donde hacerse presente suponía un reto.

Sindominio se creó en este contexto. Con deseos de horizontalidad, de ruptura entre clientes y administradores, empoderándonos tecnológicamente.

En estos 19 años muchas cosas han cambiado. Internet ha pasado a ser un espacio privado de unas pocas corporaciones, en el cual nos han acostumbrado a una cultura de falsa gratuidad, donde ya no pagamos con dinero los servicios que usamos en la red sino con nuestra privacidad.
A donde queremos ir

Entendemos que internet no es como era y que necesitamos reconstruirnos para adaptarnos a las nuevas realidades. Queremos seguir dando servicios de confianza y seguros para la comunidad que nos rodea (activistas, comunidades autogestionadas, entes pensantes, …). Pero reduciendo la barrera de entrada para acceder a servicios de Sindominio. Sin ello dejar de ser un espacio de empoderamiento tecnológico, donde dejar atrás la dicotomía de las socis y las tequis para aprender y construir juntas.

### Pertenencia a Sindominio

Hay dos formas esenciales de pertenencia a Sindominio: como Sindominante, y como Amiga.

Las Sindominantes son las que forman parte de las decisiones del día a día, mantienen Sindominio funcionando y tienen acceso a todos los recursos del servidor.

Por otra parte, las amigas han sido invitadas por Sindominantes para que cogestionen y/o puedan usar algunos de los recursos virtuales de Sindominio.

#### Sindominantes

Las Sindominantes son las que conforman la asamblea y los grupos de trabajo. Toda Sindominante debe estar subscrita y ser partícipe de la asamblea y se espera de ella que forme parte de algún grupo de trabajo.

La entrada de una nueva Sindominante requiere de una Sindominante que la avale y el consenso de la asamblea. Una madrina (normalmente quien la avala) se encargará de hacer el acompañamiento de la nueva Sindominante introduciéndola al funcionamiento de Sindominio y ayudándola con sus dudas.

Las Sindominantes tienen acceso a todos los servicios de Sindominio sin restricciones y pueden crear cuentas de amigas sin pasar por la asamblea.

Además, las listas de correo se podrán crear sin pasar por la asamblea, pero se notificará a la asamblea de su creación.

En cambio las páginas web se continuarán solicitando aparte, ya que son contenidos públicos y puede haber problemas legales. Se darán solo a las Sindominantes que las pidan, participen en lista asamblea y/o en algún grupo de trabajo.

#### Amigas

Una Amiga es una cuenta reducida en Sindominio. Con acceso a algunos de los servicios de Sindominio: correo electrónico, jabber, chat...

Las Amigas no necesitan ser aprobadas por la asamblea, toda Sindominante puede introducir amigas directamente. La Sindominante que ha introducido a cada amiga es su responsable.

### La asamblea

La asamblea es el principal espacio de toma de decisiones. Es telemática a través de una lista de correo.

Todas las Sindominantes forman parte de la asamblea.

La toma de decisiones en la asamblea es por consenso. Una propuesta que lleva una semana sin que nadie diga nada en contra se considera aprobada. Una propuesta con un único veto queda directamente rechazada.

### Grupos de trabajo

Sindominio se organiza en grupos de trabajo. Cada faceta o recurso tiene su grupo de trabajo. La entrada de cada Sindominante en un grupo de trabajo pasa por la asamblea, informando de ello y permitiendo a las Sindominantes discutirla.

Una vez cada 6 meses, cada grupo de trabajo tiene que enviar un informe a la asamblea donde se mencionen las cosas que se han hecho en ese periodo y se listan las participantes del grupo. Puede ser un email corto y rápido. Estos informes le sirven a la asamblea para valorar si un grupo de trabajo sigue vivo o si hace falta cerrarlo o ver como revivirlo.

Los grupos de trabajo tienen autonomía para decidir si una decisión necesita ser aprobada por la asamblea, notificada a la asamblea o esperar al informe semestral para mencionarla.

Es responsabilidad de cada grupo de trabajo mantener un listado de habilidades que puedes aprender formando parte de el, y ayudar a las sindominantes nuevas en el grupo a aprenderlas.

### ¡A por otras dos décadas (al menos!) de un dominio antagonista en la red!

Si con todo esto aún tienes alguna duda u opinión, o simplemente quieres felicitarnos por mantener vivo este proyecto, puedes contactarnos a través de este correo: sd@sindominio.net



[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)

