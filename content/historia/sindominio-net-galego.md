---
title: "sinDominio.net: Por un domínio antagonista na internet, pola necessidade da comunicación livre" 
---
<img src="../../img/sd_zona_temporalmente_autonoma.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

Miércoles 1 de octubre de 2003.

Como sabemos, existem desde há anos no Estado Espanhol servidores telemáticos sem ánimo de lucro -federados no

Ipanex– que se oferecem como provedores de conexom para os colectivos, organizaçons e pessoas do ámbito da esquerda social e política, que podem assim publicar os seus conteúdos sem depender de um servidor comercial ou institucional. Neste terreno figerom um grande trabalho, dando acesso à Internet e permitindo a visibilidade de bastantes organizaçons e colectivos, mas bem é certo que a maioria do ámbito das ONGs. Apostar por oferecer conectividade é umha opçom, mas nom é a única. É mais, algumhas e algúns pensamos que a reivindicaçom da livre conectividade e o “acesso para tod@s” vai perdendo transcendência na medida em que o interesse estratégico do capital em oferecer serviços através da Internet precisa que a comunidade esteja conectada.

Evidentemente, falamos do norte rico; no chamado Terceiro Mundo a situaçom actual é mui diferente (só umha de cada cinco pessoas dispom de linha telefónica), mas a tendência neoliberal do “acesso para tod@s” é global e imparável.
Conteúdos versus conectividade? Política versus infra-estruturas? Pola inteligência colectiva

Isso nom implica que nom continuemos promovendo o acesso livre para tod@s (por exemplo, desde centros sociais ocupados e outros espaços autogerenciados), mas com isso nom queremos dizer “acesso à parte material” -a singela conexom técnica, que cada vez é mais barata e mais universal. Mais bem queremos dizer um acesso para todo o mundo aos processos de inteligência colectiva, é dizer, ao ciber-espaço entendido como sistema aberto e cooperativo de expressom das singularidades, de determinaçom dos problemas, toma das decisons, em fim, de recomposiçom de vínculos sociais através da aprendizagem recíproca e da livre circulaçom do saber.

Porque o ciber-espaço tem umha potência que ultrapassa muito a possibilidade de publicar conteúdos mais ou menos alternativos ou críticos, e nos oferece a possibilidade da comunicaçom horizontal e de interconectar redes, projectos, luitas, desejos e realidades. Quer dizer, nom só é um lugar onde espalhar aquelas mensagens que noutros meios som totalmente silenciadas, senom que em si próprio permite levar a cabo práticas políticas que até agora só precariamente e a nível mui local eram possíveis. Porque o ciber-espaço (digamo-lo mais umha vez) nom é umha ferramenta, nom é umha infra-estrutura existente, em definitiva, o ciber-espaço é um jeito particular de relaçom entre pessoas, um verdadeiro movimento social que se desenvolveu à margem de estados e multinacionais sobre umha base de funcionamento cooperativo. Mas que de nengumha maneira se entenda isto como um intento escapista de que a gente abandone os seus territórios e as suas luitas para se perder num “mundo virtual” (também nom desejamos que um mundo imite ao outro) senom mais bem que se empregue a virtualidade para habitar melhor o território, para nos comunicar e construir socialidade autodeterminada e nom determinada polo Estado nem polas instituiçons. O ciber-espaço deve-nos servir também para coordenar-nos melhor, para impulsionar debates, campanhas e acçons conjuntas, em fim, para pôr em concerto a diversidade e experimentar formas de cooperaçom, de escuita mútua e de democracia inéditas até agora.
Quê quer Sindominio?

Sindominio pretende submergir-se em todo esse multiverso que se auto-organiza e mover-se pola rede, fazer visíveis e potenciar as realidades antagonistas que agora se acham dispersas ou fora da rede e aportar o que poda a esse espaço de cooperaçom e comunicaçom, e também de conflitos e luitas, onde já há postos em pé projectos autogerenciados dumhas dimensons extraordinárias, como é por exemplo o que deu lugar ao sistema operativo GNU/Linux, o maior exponente – junto à própria rede Internet- de construçom colectiva através do ciber-espaço. Precisamos umha máquina Linux onde experimentar com ferramentas livres e cooperar no estratégico campo do software.

Sindominio também pretende coordenar-se e cooperar a fundo com projectos similares ao nosso, como é o caso da ECN italiana onde a relaçom e a implicaçom entre o projecto telemático e as realidades que se coordenam nele é total, e onde a questom de competir com outros servidores em serviços, tarifas, etc… é supérflua porque nom oferecem conectividade: precisamos que quem participe em sindominio nom se converta num ou numha cliente ou num/numha singela@ usuária@ a quem dar um serviço em troque polos seus quartos. Sindominio é um projecto militante e deve suster-se com aportaçons económicas, sem assalariar@s, liberad@s ou pessoas que devam adicar-se a tempo completo. Isto obriga a gerar umha cultura diferente, menos passiva, e o emprego dos computadores entre a gente que se move com critérios alternativos noutras questoms, rachar com a ideia de “dar serviços”, dotar de elementos de juízo para situar- -se criticamente perante os empregos banais, comerciais ou despolitizados da Internet e aprender todo o bom das comunidades virtuais e da sua aposta pola inteligência colectiva.

Mas sobre todo Sindominio só e viável e interessante se se emprega como um recurso do movimento antagonista. Ou seja, como ferramenta da comunicaçom alternativa e para a coordenaçom e cooperaçom daqueles colectivos e pessoas que luitem pola autogerência e promovam a autonomia do social nos ámbitos mais diversos. Nesse senso, Sindominio oferecerá a base material a projectos como a agência em permanente construçom da UPA e contrainfos ou ao centro de documentaçom antagonista, além de listas de correio, e-mail e espaço web a todas aquelas realidades antagonistas (é dizer autogerenciadas e de base, nom institucionais nem partidárias) que desejem implicar-se no projecto Sindominio.
Em quê consiste Sindominio?

A ideia do projecto Sindominio é ter umha máquina ligada as 24 horas do dia à Internet, e visível polo tanto desde qualquer lugar do mundo com acesso a rede de redes. A máquina albergará um domínio -sindominio.net-. Um domínio é um endereço fixo na Internet representado por um nome que qualquer máquina desde qualquer parte do mundo pode localizar, por exemplo para enviar um correio electrónico ou para solicitar ver o conteúdo dumha página web. Em concreto, o nome de domínio adoita corresponder-se com a parte direita da arroba dum endereço de correio electrónico.

Sobre a nossa máquina GNU/Linux funcionaram umha série de serviços: servidor de páginas web, servidor de correio electrónico, listas de correio, news (grupos de debate), buscadores, arquivo e centro de documentaçom, a agência de novas contr@infos, mirrors (ou seja, cópias de servidores que protegem de seqüestros e aliviam o tráfico na rede)… Nom há mais límite que o do nosso conhecimento, nom há mais límite que o da nossa imaginaçom e o nosso desejo de aproveitar estes recursos: quem participe em Sindominio terá plena disponibilidade da máquina. É dizer, nom haverá a típica divisom estritamente mercantil entre conteúdos e serviços (e que se resume assim: “Coloca as tuas notícias, que nos che cobramos os serviços”).

O GNU/Linux permite umha administraçom remota completa e fai de todo indiferente à localizaçom física da máquina, que poderá ser administrada e gerenciada desde vários lugares simultaneamente.
Como participar no projecto

Nos recentes Encontros de Contrainformaçom celebrados em Madrid (http://www.nodo50.org/contrainfos), vários colectivos procedentes de diversos pontos da península decidimos ir adiante com este ambicioso projecto. Pola sua parte, @s companheir@s da ECN colocarom à nossa disposiçom umha lista de correio desde a qual estamos coordenando-
-  nos. Se queres informar-te melhor ou participar nela, escreve-nos.

O jeito de apoiar ou participar em Sindominio é com umha aportaçom de 30 euros ao ano, ainda que para começar estaria bem que quem pudera ponha mais quartos, independentemente do emprego que se pense fazer da máquina num primeiro momento. Estes quartos serviram para afrontar os gastos: o da máquina e o do aluguer anual a um provedor dumha linha adicada (entrada e saída a Internet) e o espaço físico onde pôr a maquina (housing).

**O numero de conta é: 3025 0006 21 1433230004 de a Caja de Ingenieros**
*(importante que indiqueis quem (colectivo, etc…) fai o ingresso)*

[www.sindominio.net](https://sindominio.net)

[sd@sindominio.net](mailto:sd@sindominio.net)

</br>
</br>
</br>
**É necessário explicar a nossa escolha ortográfica?**

Somos conscientes de que toda escolha ortográfica supom um posicionamento mas quando figemos esta escolha nem pensamos em explicá-lo. No momento de constituir-se SD as pessoas galegas no projecto tiverom claro que a normativa AGaL era a grafia que mais se ajustava ao seu pensamento político e foi a que escolherom para a página de apresentaçom de SD. Posteriormente, ao ser questionada esta postura desde fora do projecto e com pessoas em SD que nom estiveram no início, decidimos abrir um debate sobre se mantínhamos a escolha. Havia diferentes posturas, desde questionar o galego como língua distinta da portuguesa, até a ideia de fazer distintas páginas nas distintas normativas. Finalmente decidimos manter a escolha, contar-vos isto e dar-vos vários pontos de vista no debate para que vos formedes vós a vossa postura pondo ligações a várias páginas web:

Em http://membres.lycos.fr/questione/ debate-se sobre as normativas e outros temas a respeito da língua. O colectivo que elaborou a normativa na que está esta página, AGaL, tem a seguinte página: http://www.agal-gz.org/. Em SD está alojado o Movimento Defesa da Língua http://sindominio.net/mdl/.

SD é um projecto plural e baseado em assembleia no que cada decisom política é tomada polas pessoas que fazemos parte deste colectivo. Neste caso, ao tratar-se do nosso idioma, foi tomada polas pessoas galegas no projecto.
