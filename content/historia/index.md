---
title: "Historia"
date: 2019-10-13T10:53:16+02:00
draft: false
---

### Textos publicados a lo largo del tiempo

* [sinDominio.net: una apuesta por la telemática antagonista y por la inteligencia colectiva (2003).](una-apuesta-por-la-telematica-antagonista-2003)

* [No se puede decir que diez años no son nada. (2009)](no-se-puede-decir-que-diez-anyos-no-son-nada-2009)

* [Queridas majestades del precariado republicano. (2016)](queridas-majestades-del-precariado-republicano-2016)

* [A por dos decadas libres de dominio. (2018)](a-por-dos-decadas-libres-de-dominio-2018)


### Y en otros idiomas

* Euskara: [sinDominio.net: telematika antagonistaren eta adimen kolektiboaren aldeko erronka](sindominio-net-euskara)

* Català: [sinDominio.net: una aposta per la telemàtica antagonista i per la intelligència collectiva](sindominio-net-catala)

* Galego: [sinDominio.net: Por un domínio antagonista na internet, pola necessidade da comunicación livre](sindominio-net-galego)

* English: [sinDominio.net: placing our bets on dissident Telematics and ‘general intellect’](sindominio-net-english)

* Français: [sinDominio.net: Un pari pour la télématique antagoniste au bénéfice de l’intelligence collective](sindominio-net-francais)

* Italiano: [Alcune questioni attorno a sinDominio.net](sindominio-net-italiano)

* Português: [sinDominio.net: uma aposta na telemática antagonista e na inteligéncia coletiva](sindominio-net-portugues)

* Deutsch: [sinDominio.net: häufig gestellte Fragen](sindominio-net-deutsch)

* Esperanto: [sinDominio.net: Veto por la alternativa telematiko kaj por la kolektiva inteligenteco](sindominio-net-esperanto)


###  Cuéntame la historia de Sindominio.net

En los primeros Encuentros de Contrainformación del Estado Español, celebrados en Zaragoza se habla de montar un servidor, sin embargo, dado los escasos conocimientos del medio que había por aquel entonces la idea no llegó muy lejos. Pero sí sirvió para construir la Red Contr@-Infos, y el acuerdo de realizar Encuentros de Contrainformación periódicamente, que como se verá, jugarían un papel decisivo en el lanzamiento de Sindominio.

La idea volvería a surgir en Madrid durante unas jornadas sobre nuevas perspectivas de los centros sociales, en los últimos días de septiembre de 1998, celebradas en el CSOA el Laboratorio de Embajadores. En aquella ocasion, se discutieron en varias sesiones las nuevas tecnologías, la telemática, etc. A estas jornadas asistió gente de diversos Centros Sociales Okupados y Autogestionados de Barcelona y Madrid, y se acordó difundir la idea y hacer avanzar el proyecto.

Poco más tarde ya se habían unido a la iniciativa más de 25 colectivos y centros sociales de todo el Estado. En los cuartos Encuentros de Contrainformación (http://www.nodo50.org/contrainfos) también celebrados en el CSOA el Laboratorio (pero esta vez en el ubicado en el parque de Cabestreros) a fecha de Abril de 1999 recibió un impulso todavía mayor, con el apoyo de todo el tejido contrainformativo del Estado Español. En aquellos momentos, todavía nos encontrábamos sin máquina, sin el dominio registrado, sin ISP que nos permitiera conectar nuestra máquina a su red, sin dinero. A todo esto se unía que la experiencia que teníamos sobre el aspecto técnico era bastante pobre, así que debíamos esforzarnos -y mucho- para poder alcanzar el objetivo.

“Isole Nella Rete” (http://www.ecn.org) tuvo la gentileza de hospedarnos una lista de correo, gracias a la cual pudimos coordinarnos y crear una asamblea permanente en la que han participado tod@s, y aún más importante, en la que se ha hablado de todo. Nunca dudamos que no debíamos separar la parte técnica de la política; y aunque para algunas personas podía resultar muy duro oir hablar de TCP/IP o Debian, creemos que era fundamental crear un espacio para socializar el conocimiento, y pensamos que una manera de hacerlo era en nuestra asamblea.

Tan importante como la lista que nos proporcionó la ECN, fueron los consejos que nos dieron del tipo de ISP que debíamos buscar (nos describieron algo como kender, un ISP pequeño y linuxero) y cómo tenian implementados los servicios en la ECN y como se distribuian las responsabilidades (la idea de varios roots cada uno con servicios asignados, y la lista de admin proceden de la reunion con jerry cornelius, uno de los roots de la ECN por entonces).

TAO (The Anarchy Organization, http://www.tao.ca) nos hizo un gran favor cuando registramos nuestro dominio, ofreciendo sus servidores de nombres (DNS) y albergando provisionalmente nuestras páginas en su servidor.

Más tarde, en Julio de 1999, se compró la máquina, un Pentium II a 450 MHz, 256 Mb de RAM y 9 Gb de disco duro, que hemos llamado Fanelli, nombre de un viejo anarquista que difundió en la península ibérica la Primera Internacional. En una asamblea en Madrid se realizó una instalación conjunta, en la que tod@s aportaron su trabajo, pero en la que sobre todo se aprendió mucho sobre cómo es un servidor y cómo funcionan los distintos servicios. Ahora sólo faltaba un ISP y, como siempre, algo de dinero. Pensamos, ya que la configuración de la máquina no se podía hacer sin la colaboración de todas las personas implicadas en el proyecto, en conectarla via PPP unas horas al día, aunque esto serviría solamente para hacer algunas pruebas.

Después de haber discutido los precios de varios ISP, la oferta más interesante pareció fue la que nos habían comentado la ECN: kender, con el que además un compañero nuestro tenía una relación personal. El problema era que en agosto no podían atendernos, así que pasamos todo este tiempo configurando la máquina y discutiendo si sería mejor un servidor de correo antes que otro, etc.

Finalmente, en octubre de 1999 celebrábamos el gran acontecimiento, ya no éramos simplemente un proyecto, sino una realidad: nuestra máquina estaba finalmente conectada y operativa. El camino había sido difícil y no exento de obstáculos, pero gracias a nuestro empeño y al apoyo recibido tanto del movimiento en el Estado Español, como de proyectos ya asentados como INR y TAO, lo conseguimos.

Y pronto, la mayoría de los servicios quedaron desde ese momento plenamente operativos: correo, servidor web, FTP, SSH, listas de correo… Y un de los proyecto que hizo a SinDominio realmente popular por todos los movimientos sociales dentro del mundo hispano y fuera de él también fue la Agencia en Construcción Permanente, un servicio de noticias en tiempo real, con la peculiaridad de que no sólo se pueden colocar y leer noticias, sino que éstas se pueden comentar, ampliar, debatir…

Estuvo operativa de este modo de enero a noviembre de 2000, pero desgraciadamente, después de varios meses de funcionamiento comprobamos que no satisfacía nuestras espectativas, sino que muy al contrario era una fuente de problemas, tanto técnicos como políticos:

El software utilizado dió un rendimiento muy inferior al esperado. Poco flexible, lento y consumía gran cantidad de recursos.
El contenido de la ACP, en líneas generales, no brillaba precisamente por su calidad, y se alejaba cada vez más de los objetivos con los que fue creada.

Por lo que pasamos a cerrarla, analizar errores y reabrirla. Este proceso ha culminado en Abril del 2002 dónde disfrutamos de una ACP, que es un proyecto autónomo de sindominio (con su propia lista de toma de decisiones), dentro de la red internacional de contrainformación Indymedia y con un software basado en SlashCode, que es el que utiliza la comunidad del software libre para intercambiar sus noticias.

Sin embargo, no todos los problemas de SinDominio eran la ACP. SinDominio había crecido de una manera que le impedía cumplir sus propios objetivos. Había mucha gente con cuenta en SinDominio, pero muy pocos se habían implicado en SinDominio ya fuera en tareas técnicas ó sociales, de hecho incluso había cuentas que no se leían desde hacía más de seis meses. Tal vez en este SinDominio no había empresarios, pero sí había actitudes que impedían hacer crecer a SinDominio como una comunidad, que impedían que se hiciera una administración horizontal. Muchos grupos entraban a sindominio sin saber muy bien donde entraban (muchos se quejaban de que se recibia mucho correo y otros afirmaban no poder conectarse nunca), concebían SinDominio un espacio donde “hacer política”, no un espacio donde construir una comunidad y que había que cuidar entre todos. Es decir, bastantes grupos lo veían como un sitio donde había “mucha peña” a la que difundir sus infos, o pedir recursos, donde extender sus campañas, etc. pero nada más. Era como si sindominio fuese una “institución” que funcionaba sola, es decir, algo ya dado, y se daban debates al estilo de las coordinadoras sin voluntad constructiva, en las que cada grupo intenta llevarse el gato al agua.

Fueron meses de duros debates en los que hubo mucha gente que se fue de sindominio enfadada, en los que hubo que congelar el alta a nuevos miembros, ya que primero deberíamos aclararnos. Pero poco a poco las cosas se fueron calmando y con el refuerzo de una asamblea presencial en Barcelona en Abril del 2001 se hizo lo que se llamó la “Refundación de SinDominio”. A partir de esta Refundación se vió que:

La lista asamblea se compondría de personas y en ningún caso de colectivos. La razón es que los ritmos de las asambleas de colectivos difieren mucho de los ritmos de las listas de distribución, no digamos ya los ritmos de coordinadoras ó federaciones.
Si una cuenta no se usa durante más de cuatro meses debe ser anulada. Alguien que no puede leer su cuenta de correo durante tanto tiempo no puede seguir la vida de SinDominio.
Todas las personas que compusieran SinDominio deberían responsabilizarse del espacio común y de su espacio particular. Esto supondría que en caso de problemas legales este problema se socialice lo más posible.
SinDominio debería constituirse como Asociación, para evitar problemas legales.

Ese proceso de Refundación ya se ha conseguido. Hoy sí se puede hablar de SinDominio como una comunidad, a pesar de que existan muy diferentes grados de implicación en la misma, en general, existe preocupación por el construir comunmente. La ACP ha vuelto como un proyecto autónomo, con su propia lista de decisión, hemos tenido que comprar una nueva máquina por habernos quedado sin recursos debido a la gran cantidad de proyectos que han surgido, se experimenta y se debate acerca de cómo distribuir/horizontalizar todo lo posible las tareas de administración.   
</br>
</br>
En fin, vivimos tiempos felices que nos gustaría compartir contigo, si quieres ;).

