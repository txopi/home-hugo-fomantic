---
title: "sinDominio.net: uma aposta na telemática antagonista e na inteligéncia coletiva" 
---
<img src="../../img/sd_zona_temporalmente_autonoma.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

Jueves 5 de febrero de 2004.

Como se sabe, existem há muitos anos na Espanha, servidores sem fins lucrativos – federados no Ipanex– que se oferecen como provedores de Internet para os coletivos, organizações e pessoas da esquerda social e política, que podem, desta forma, publicar seus conteúdos sem depender de um servidor comercial ou institucional. Nesse campo têm realizado um grande trabalho, fornecendo acesso à Internet e permitindo sua divulgação a diversas organizações e coletivos, em sua maioria ONGs. Apostar em oferecer acesso é uma opção, porém não é a única opção. É mais, nós cremos que a reivindicação pela conexão gratuita e o “acesso para tod@s” vai perdendo intensidade à medida que aumenta o interesse estratégico do capital em oferecer serviços através da Internet e necessita que a comunidade esteja conectada. Evidentemente, estamos falando do norte rico, pois no chamado terceiro mundo a situação atual é muito diferente (apenas uma em cada cinco pessoas possui linha telefônica), mas a tendência neoliberal do “acesso para tod@s” é global e indetenível.

### Conteúdos versus conectividade? Política versus tubulações? Pela inteligência coletiva

Isto não nos livra de seguir apoiando o acesso livre para tod@s (por exemplo, de centros sociais ocupados e outros espaços autogerenciados), mas com isto não queremos dizer “acesso à parte material” (a simples conexão técnica, que cada vez está mais barata e mais universal), nem “acesso ao conteúdo” (consumo de informações e de conhecimentos difundidos unilateralmente). Queremos dizer um acesso para todo o mundo dos processos de ingeligência coletiva, é dizer, ao cyber-espaço tido como um sistema aberto e cooperativo de expressão das singularidades, da identificação de problemas, da tomada de decisões, em fim, de recomposição de vínculos sociais através da aprendizagem recíproca e da livre circulação do saber.

Em verdade, o cyber-espaço tem uma potência que sobrepassa em muito a possibilidade de publicar conteúdos mais ou menos alternativos ou críticos, e nos oferece a possibilidade da comunicação horizontal e de interconectar redes, projetos, lutas, desejos e realidades. Isto é, não só um lugar onde difundir aquelas mensagens que outros meios são totalemente silenciados, mas que em sí mesma permite levar a cabo práticas políticas que até agora só precariamente e em um nivel muito local era possível. Porque o cyber-espaço – afirmemos uma vez mais – não é uma ferramenta, não é uma infraestrutura: é um determinado modo de utiliazr as infraestruturas existentes, em suma, o cyber-espaço é um tipo particular de relação entre pessoas, um verdadeiro movimento social que se desenvolveu à margem de Estados e multinacionais sobre uma base de funcionamento cooperativo. Mas que de nenhuma maneira se entenda isso como uma tentativa escapista de as pessoas abondonem seu território e suas lutas para se perderem num “mundo virtual” (tampouco desejamos que um mundo imite ao outro), mas que se use a virtualidade para habitar melhor o território, para comunicarnos e construir uma sociedade autodeterminada e sem o intermédio do Estado nem das instituições. O cyber-espaço nos deve servir também para coordenarmos melhor, para impulsionar debates, campanhas e ações conjuntas, em fim, para por em mostra a diversidade e experimentar formas de cooperação, de escuta mútua e de democracia inéditas até agora.

### O que sindominio quer?

Sindominio pretende submergir-se em todo este multiverso que se auto-organiza e se move pela red, facer visíveis e potenciar as realidades antagonistas que atualmente se encontram dispersas ou fora da rede e contrubuir com o que possa para este espaço de cooperaçao e de comunicação, e também de conflitos e lutas, onde já existem em andamento projetos auto-gerenciados de dimensões extaordinárias – como por exemplo o ponto a que chegou o sistema operacional GNU/Linux, o cume – junto com a própria rede – da construção coletiva através do espaço cibernético. Necessitamos de uma máquina Linux onde possa demonstrar com ferramentas livres e cooperar, pesquisar e trocar conhecimentos com as comunidades virtuais no estratégico campo do software.

Sindominio também pretende coordenar-se e cooperar a fundo com projetos similares ao nosso, como é o caso da ECN italiana, onde a relação e a implicação entre o projeto telemático e as realidades que se coordenam nele é total e onde a questão de competir com outros servidores em serviços, tarifas, etc. é supérflua por que não oferecem conectividade: precisamos que quem participe em sindominio não se converta em um cliente ou em um simples usuário a quem há de dar um serviço em troca do seu dinheiro. Sindominio é um projeto militante e deve suspender-se com contribuições econômicas, sem assalariad@s, liberad@s ou pessoas que devam dedicar-se o tempo todo. Isto obriga a criar uma cultura diferente, menos passiva, no uso dos computadores entre a gente que se move com critérios alternativos em outras questões, romper com a idéia de ‘dar serviços’, dotar de elementos de juízo para situar-se criticamente perante os usos nais, comerciais ou despolitizados da Internet e aprender tudo de bom das comunidades virtuais e de sua aposta na inteligência coletiva.

Mas, sobre tudo sindominio só é viável e interessante se se utiliza como um recurso do movimento antagonista. Ou seja, como uma ferramenta da comunicação alternativa e para a coordenação e cooperação daqueles coletivos e pessoas que lutam pela autogestão e promovem a autonomia do social nos âmbitos mais diversos. Neste sentido, sindominio oferecerá a base material a projetos como a agência em permanente construção da UPA e contr@infos ou ao centro de cocumentação antagonista, além de listas de correio, e-mail e espaço na rede para todas aquelas realidades antagonistas (isto é, autogerenciadas e de base, não institucionais nem partidárias) que queiram implicar-se no projeto sindominio.

### En que consiste sindominio?

A idéia do projeto sindominio é ter uma máquina conectada 24 horas por dia à Internet, e visível portanto de qualquer lugar do mundo com acesso à rede de redes. A máquina abrigará um domínio – sindominio.net-. Um domínio é um endereço fixo na Internet representado por um nome que qualquer máquina em qualquer parte do mundo possa localizar, por exemplo para enviar um correio eletrônico ou para ver o conteúdo de uma página. Também, o nome do domínio deve corresponder com a parte direita da arroba de um endereço de correio eletrônico.

Na nossa máquina Linux funcionarão toda uma série de serviços: servidor de espaço para páginas, servidor de correio eletrônico, listas de coreio, news (grupos de debate), motores de pesquisa, arquivo e centro de documentação, a agência de notícias de contr@infos, mirros (ou seja, cópias de servidores que protegem de seqüestros e aliviam o tráfico na rede)… não há outro limite senão o do nosso conhecimento, não há outro limite senão o da nossa imaginação e nossa vontade de aproveitar estes recursos: quem participar em sindominio terá plena disponibilidade da máquina, isto é, não haverá a típica divisão estritamente mercantil entre conteúdos e serviços (e que se resume assim: “coloque suas informações, que nós te cobramos os serviços”).

O Linux permite uma administração remota completa e faz indiferente a localiazação física da máquina, que poderá ser administrada e gerenciada de vários lugares simultaneamente.

### Como participar do projeto?

Nos recentes Encontros de Contrainformação realizados em Madrid (http://www.nodo50.org/contrainfos), vários coletivos procedentes de diversos pontos da Península Ibéruca decidimos seguir com este ambicioso projeto. Os companheir@s da ECN, por sua vez, colocaram a nossa disposição uma lista de correio a qual estamos coordenando. Se deseja informar-se melhor ou participar dela, escreva-nos: sd@sindominio.net

O modo de apoiar o sindominio é com uma contribuição de 30 euros por ano, ainda que para começar seria melhor que quem possa coloque mais dinheiro, independentemente do uso que queira fazer da máquina em um primeiro momento. Este dinheiro servirá para cobrir os gastos: o da máquina e o do aluguel anual de um provedor de linha dedicada (linha discada direta à Internet) e do espaço físico para a máquina.

**O número da conta é: 3025 0006 21 1433230004 do banco Caja de Ingenieros.**

[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)


