---
title: "sinDominio.net: una aposta per la telemàtica antagonista i per la intelligència collectiva"
---
<img src="../../img/sd_zona_temporalmente_autonoma.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

Miércoles 1 de octubre de 2003.

Des de ja fa uns quants anys existeixen a l’Estat espanyol diversos servidors telemàtics sense ànim de lucre -federats a Ipanex- que s’ofereixen com a proveïdors de connexions a Internet per als col.lectius, organitzacions i persones de l’àmbit de l’esquerra social i política, i que poden, d’aquesta manera, publicar els seus continguts sense dependre d’un servidor comercial o institucional. En aquest camp s’ha fet una gran feina, donant accés a Internet i permetent la seva visibilitat a bastantes organitzacions i col.lectius, si bé també és cert que la majoria de l’àmbit de les ONG. Apostar per oferir connectivitat és una opció, però no és l’única opoció. És més, alguns/es pensem que la reivindicació de la lliure connectivitat i l'”accés per a tots/es” va perdent trascendència en la mesura que augmenta l’interés estratègic del capital en oferir serveis a través d’Internet i necessita que la ciutadania estigui connectada. Evidentment, parlem del Nord ric; a l’anomenat Tercer Món la situació actual és molt diferent (només 1 de cada 5 persones disposa de línia telefònica), però la tendència neoliberal de l'”accés per a tots/es” és global i imparable.

### Continguts versus conectivitat? Política versus tuberia? Per la intel.ligència col.lectiva

Això no treu que continuem promovent l’accés lliure per a tots/es (per exemple, des de centres socials okupats i altres espais autogestionats), però amb això no volem dir “accés a la part material” -la simple connexió tècnica, que cada cop és més barata i més universal-, ni tampoc “accés al contingut” -consum d’informacions o coneixements difosos unívocament-. Més aviat volem dir un accés per a tot el món als processos d’intel.ligència col.lectiva, és a dir, al ciberespai entès com a sistema obert i cooperatiu d’expressió de singularitats, de determinació dels problemes, de presa de decisions, en fi, de recomposició de vincles socials a través de l’aprenentatge recíproc i de la lliure circulació del saber.

I és que el ciberespai té una potència que sobrepassa de molt la possibilitat de publicar continguts més o menys alternatius o crítics, i ens ofereix la possibilitat de la comunicació horitzontal i d’interconnectar xarxes, projectes, lluites, desitjos i realitats. És a dir, no només és un lloc on difondre aquells missatges que a d’altres mitjans són totalment silenciats, sino que en si mateix permet dur a terme pràctiques polítiques que fins ara només precàriament i a nivell local eren possibles. Perque el ciberespai -diguem-ho un cop més- no és una eina, no és una infraestructura: és una determinada manera d’utilitzar les infraestructures existents, en suma, el ciberespai és un tipus particular de relació entre persones, un veritable moviment social que s’ha desenvolupat al marge d’Estats i multinacionals sobre una base de funcionament cooperatiu. Però que de cap manera s’entengui això com un intent escapista perquè la gent abandoni els seus territoris i les seves lluites per a perdre’s en un “món virtual” (tampoc volem que un món imiti a l’altre), sino construir socialitat autodeterminada i no mediada per l’Estat ni per les institucions. El ciberespai ens ha de servir també per a coordinar-nos millor, impulsar debats, campanyes i accions conjuntes, en fi, per a posar en concert la diversitat i experimentar formes de cooperació, d’escolta mútua i de democràcia inèdita fins ara.

### Què preten SinDominio?

Sindominio preten submergir-se en tot aquest multivers que s’autoorganitza i es mou per la xarxa, fer visibles i potenciar les realitats antagonistes que ara es troben disperses o fora de la xarxa i aportar el que pugui a aquest espai de cooperació i de comunicació, i també de conflictes i lluites, on ja s’han iniciat projectes autogestionats de dimensions extraordinàries com ara, per exemple, el que ha donat lloc al sistema operatiu GNU/Linux, el major exponent -juntament a la pròpia xarxa d’Internet- de construcció col.lectiva a través del ciberespai. Necessitem un màquina Linux on poder probar eines lliures i cooperar, investigar i intercanviar coneixements amb les comunitats virtuals en l’estratègic camp del software.

Sindominio també preten coordinar-se i cooperar a fons amb projectes similars al nostre, com és el cas de la ECN italiana, on la relació i la implicació entre el projecte telemàtic i les realitats que es coordinen en ell és total i on la qüestió de competir amb d’altres servidors en seveis, tarifes, etc. és supèrflua perque no ofereixen connectivitat: necessitem qui participi a sindominio no es converteixi en un client o en un simple usuari a qui se li ha de donar un servei a canvi dels seus diners. Sindominio és un projecte militant i s’ha de mantenir amb aportacions econòmiques, sense assalariats/des, alliberats/des o persones que s’hi hagin de dedicar a temps complert. Això obliga a generar una cultura diferent, menys passiva, en l’ús dels ordinadors entre la gent que es mou amb criteris alternatius en altres qüestions, trencar la idea de “donar serveis”, dotar-se d’elements de criteri per a situar-se críticament davant dels usos banals, comercials o despolititzats d’Internet i aprendre tot allò bo de les comunitats virtuals i de la seva aposta per la intel.ligència col.lectiva.

Però per damunt de tot sindominio només és viable i interessant si s’utilitza com un recurs pel moviment antagonista. O sigui, com a eina de la comunicació alternativa i per a la coordinació i cooperació d’aquells col.lectius i persones que lluiten per l’autogestió i que promouen l’autonomia social en els àmbits més diversos. En aquest sentit, sindominio oferirà la base material a projectes com l’agència en permanent construcció de la UPA i Contr@infos o al centre de documentació antagonista, a més a més de llistes de correu, e-mail, espai web a totes aquelles realitats antagonistes (és a dir, autogestionades i de base, no institucionals ni partidàries) que vulgin implicar-se en el projecte sindominio.

### En què consisteix SinDominio?

La idea del projecte sindominio és tenir una màquina connectada les 24 hores del dia a Internet, visible per tant des de qualsevol lloc del món amb accés a la xarxa. La màquina allotjarà un domini -sindominio.net-. Un domini és un direcció fixa a Internet representada per un nom que qualsevol màquina des de qualsevol part del món pot localitzar, per exemple per a enviar un correu electrònic o per a sol.licitar veure el contingut d’una pàgina web. En concret, el nom del domini sol coincidir amb la part dreta de l’arroba d’una direcció de correu electrònic.

Sobre la nostra màquina Linux funcionaran tot a una sèrie de serveis: servidor de pàgines web, servidor de correu electrònic, llistes de correu, news (grups de debat), buscadors, arxiu i centre de documentació, l’agència de notícies contr@infos, mirrors (còpies de servidors que protegeixen de segrestaments i alleugereixen el trànsit a la xarxa)…. No hi ha cap altre límit que el del nostre coneixement, no hi ha més límit que el de la nostra imaginació i les nostres ganes d’aprofitar quests recursos: qui participi a sindominio tindrà plena disponibilitat de la màquin, és a dir, no hi haurà la típica divisió estrictament mercantil entre continguts i serveis (i que es resumeix així: “penja les teves informacions, que nosaltres et facturem els serveis”).

Linux permet una administració remota completa i fa del tot indiferent la localització física de la màquina, que podrà ser administrada i gestionada des de diferents llocs de forma simultànea.

### Com participar en el projecte?

Durant els recents Encuentros de Contrainformación celebrats a Madrid (http://www.nodo50.org/contrainfos), diversos col.lectius procedents de diferents punts de la península vam decidir tirar endavant aquest ambiciós projecte. Per la seva part, els companys/es de la ECN han posat a la nostra disposició una llista de correu des de la qual ho estem coordinant. Si vols informat-te millor o participar en ella, escriu-nos a sd@sindominio.net

La manera de recolzar i participar a sindominio és amb una aportació de 30 euros l’any, tot i que per començar estaria bé que qui pugui posi més diners, independentment de l’ús que pensi fer de la màquina en un primer moment. Aquests diners serviran per afrontar dues despeses: la de la màquina i la del lloguer anual a un proveïdor d’una línia dedicada (entrada i sortida a Internet) i l’espai físic on posar la màquina (housing).

**El número de compte és: 3025 0006 21 1433230004 de la Caja de Ingenieros.**
*(important, poseu qui (persona, col.lectiu, etc..) fa l’ingrés)*

[www.sindominio.net](https://sindominio.net)

[sd@sindominio.net](mailto:sd@sindominio.net)



