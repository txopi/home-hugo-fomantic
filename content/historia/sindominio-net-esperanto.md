---
title: "sinDominio.net: Veto por la alternativa telematiko kaj por la kolektiva inteligenteco"
---
<img src="../../img/sd_zona_temporalmente_autonoma.jpg" alt="sindominio.net ZONA TEMPORALMENTE AUTÓNOMA" style="width:100%;"/>

Jueves 25 de diciembre de 2003.

### Veto por la alternativa telematiko kaj por la kolektiva inteligenteco

Kiel sciate, ekzistas de antaŭ kelkaj jaroj en la hispana Ŝtato komputilaj serviloj sen monprofitaj celoj kiuj proponas sin kiel provizistoj de retaliroj por la kolektivoj, organizaĵoj kaj homoj de la socia kaj politika maldekstro, kiuj povas tiel publikigi siajn enhavojn sendepende de komercaj aŭ instituciaj serviloj. En tiu kadro ili faris grandan laboron, ebligante aliron al Interreto kaj la diskonigadon de sia propra ekzisto al pluraj organizaĵoj kaj kolektivoj, plejmulte devenantaj de la NROaro. Veti por oferi alireblecon estas eblo, sed ne la nura eblo. Krome, kelkaj el ni pensas ke la batalo por la libera retaliro kaj la “aliro por ĉiuj” perdas gravecon dank’al la iom-post-ioma pligranda intereso de la kapitalo por oferi servojn per la Reto, pro tio ke ĝi (la kapitalo) bezonas civitanaron kiu disponu de retaliro.

Evidente, ni parolas pri la riĉa Nordo, en la tiel nomita Tria Mondo la nuntempa situacio estas tute alia (nur 1 el ĉiu 5 homoj disponas de telefona linio), sed la novliberala tendenco al “retaliro por ĉiuj” estas globala kaj nehaltigebla.

### Ĉu enhavoj kontraŭ retaliro? Ĉu politiko kontraŭ ilo? Por la kolektiva inteligenteco.

Tio ne forigas la fakton ke ni daŭre promocias la liberan aliron por ĉiuj (ekzemple, el sociaj okupitaj ejoj kaj aliaj sinorganizataj spacoj), sed per tio ni ne celas “fizikan aliron” -nuran teknikan ŝalton, kiu tago post tago estas pli malmultekosta kaj universala-, kaj ankaŭ ne “aliron al la enhavo” -konsumon de informo aŭ konoj disvastigitaj unuflanke-. Kion ni celas estas aliro por ĉiuj homoj al la procezoj de kolektiva inteligenteco, tio estas, al la ciberspaco komprenita kiel malfermita sistemo kaj kooperativo de esprimado pri la specifaĵoj, pri determino de la problemoj, pri decidprocedoj, nu, pri la rekonstruo de sociaj ligiloj per la reciproka lernado kaj per la libera cirkulado de la kono.

La ciberspaco havas potencon kiu estas multe pli ol la nura eblo publikigi enhavojn pli-malpli alternativajn aŭ kritikajn, kaj oferas nin plene la eblon interkomuniki horizontale kaj interligi retojn, projektojn, luktojn, revojn kaj realaĵojn. Tio estas, ĝi ne nur estas loko kie diskonigi tiajn mesaĝojn kiuj en aliaj medioj estas tute silentigitaj, sed per si mem ebligas realigi politikajn praktikojn kiuj ĝis hodiaŭ nur eblas realigi malcerte kaj je loka nivelo. Ĉar la ciberspaco (ni diru tion denove) ne estas ilo, ne estas infrastrukturo: ĝi estas konkreta maniero uzi la ekzistantajn infrastrukturojn, konklude, la ciberspaco estas konkreta speco de rilato inter homoj, ĝi estas vera socia movado kiu disvolviĝis marĝene al Ŝtatoj kaj multnaciaj firmaoj sur la bazo de kooperativa funkciado. Sed ĉio ĉi neniel devas esti komprenita kiel eskapisma intenco por ke la homoj forlasu siajn teritoriojn kaj batalojn por perdiĝi en virtuala mondo (ni ankaŭ ne volas ke unu mondo limigu la alian), sed male, por ke oni uzu la virtualecon por okupi pli bone la teritorion, por interkomunikiĝi kaj konstrui sociecon sindeterminatan kaj ne peratan de la Ŝtato nek de la institucioj. La ciberspaco devas servi nin por plibona kunordigado, por starigo de kunaj debatoj, kampanjoj kaj agadoj, nu, por akordigi la diversecon kaj sperti novajn specojn de kunlaboro, de reciproka aŭskultado kaj de demokratio neekzistantaj hodiaŭ.

### Kion celas sindominio?

Sindominio celas mergi sin en tra tiun universon kiu organizas sin kaj moviĝas tra la reto, videbligi kaj potencigi la alternativajn realaĵojn kiuj nun troviĝas disaj aŭ for de la reto kaj kontribui al tiu spaco de kunlaboro kaj komuniko, sed ankaŭ de konfliktoj kaj bataloj, kie jam ekzistas eksterordinare grandaj sinorganizitaj projektoj, ekzemple tiu kiu naski s la operaciadan sistemon GNU/Linukso, la plej grandan simbolon -kune kun la propra reto- de kolektiva konstruado per la ciberspaco. Ni bezonas GNU/Linuksan maŝinon kie pruvi per liberaj iloj, kunlabori, esplori kaj interŝanĝi konojn kun la virtualaj komunumoj en la strategia kampo de la softvaro.

Sindominio ankaŭ celas kunordigi sin kaj kunlabori plene kun similaj projektoj, kiel la itala ECN, kie la rilato kaj la implikiĝo inter la interreta projekto kaj la realaĵoj kiuj kunordigas sin per ĝi estas tuta kaj kie la problemo konkuri kun aliaj serviloj pri servoj, prezoj ktp estas io superflua ĉar ili ne proponas aliron: ni bezonas ke tiuj kiuj partoprenos en sindominio ne iĝu klientoj aŭ simplaj uzantoj al kiuj ni devos doni servojn pro mono. Sindominio estas partoprena projekto kiu devas postvivi dank’al ekonomiaj donacoj, sen dungitoj nek dependaj laboristoj. Tio igas devigaj la aperon de malsama kulturo, malpli pasiva, en la uzo de la komputiloj inter la homoj kiuj havas alternativajn kriteriojn pri aliaj problemoj, la frakason de la ideo “realigi komercajn servojn”, la disdonadon de juĝelementoj por kritike rigardi la negravajn, komercajn aŭ senpolitikajn uzojn de Interreto kaj la lernadon de ĉio bona apartenanta al la virtualaj komunumoj kaj de ilia veto por la kolektiva inteligenteco.

Sed ĉefe sindominio nur estos interesa kaj povos postvivi se oni uzas ĝin kiel ilo de la alternativa movado. Tio estas, kiel ilo de la alternativa komunikado kaj por la kunordigo kaj kunlaboro de tiuj kolektivoj kaj homoj kiuj batalas por la sinorganizo kaj defendas la aŭtonomecon de la sociaj aferoj en la plej diversaj agadkampoj. Tiusence, sindominio oferos la fizikan bazon por projekto kiel la ĉiam plukonstruata novaĵagentejo de la UPA kaj contr@infos aŭ la ejo de alternativa dokumentaro, krome ĝi oferos retlistojn, retpoŝtejojn kaj ttt-ejan spacon por ĉiaj alternativaj realaĵoj (tio estas, sinorganizitaj kaj sendependaj de partioj kaj institucioj) kiuj volos partopreni en la projekto sindominio.

### En kio konsistas sindominio?

La ideo de la projekto sindominio estas disponi de maŝino konektita al la reto 24 horojn ĉiutage, kaj videblaen ĉiu loko de la mondo kun interretaliro. La maŝino enhavos domajnon -sindominio.net-. Domajno estas fiksita adreso en Interreto reprezentita de nomo kiun ĉiu ajn maŝino ĉie ajn en la mondo povas trovi, ekzemple por sendi retmesaĝon aŭ por vidi ttt-ejon. Konkrete, la nomo de domajno kutime koincidas kun la dekstra flanko de la retpoŝtaj adresoj (malantaŭ la heliko).

En nia GNU/linuksa maŝino funkcios aro da servoj: serviloj por ttt-ejoj, retpoŝta servilo, retlistoj, novaĵgrupoj, serĉiloj, arÄ¥ivo kaj dokumentejo, novaĵagentejo contr@infos, speguloj (tio estas, kopioj de serviloj kiuj evitas sekvestrojn kaj malpliigas la trafikon en la reto)… Ne estas alia limigo ol tiu de nia kono, ne estas alia limigo ol tiu de nia imago kaj niaj voloj profiti de tiuj rimedoj: kiu partoprenos en sindominio havos tutan disponeblecon de la maŝino, tio estas, ne okazos la tipa divido strikte komerca inter enhavoj kaj servoj (kaj kio eblas resumi tiele: “alŝutu viajn informojn, ke ni fakturos la servojn”).

GNU/Linukso ebligas telematikan kaj kompletan administradon kaj malgravigas la fizikan lokon de la maŝino, kiu povos esti administrita el malsamaj lokoj samtempe.

### Kiel partopreni la projekton?

En la ĵusaj Renkontiĝoj pri Kontraŭinformado okazintaj en Madrido (http://www.nodo50.org/contrainfos) kelkaj kolektivoj devenintaj el diversaj lokoj de la Iberia Duoninsulo decidis antaŭenigi ĉi tiun ambician projekton. Aliflanke, la gekunuloj de la ECN disponigis al ni retliston per kiu ni kunordigas ĝnin. Se vi volas informiĝi plibone aŭ partopreni ĝin, skribu nin.

La maniero apogi sindominio estas per ekonomia kotizo de 30 eŭroj jare, kvankam je la komenco estus bone ke tiuj kiuj povos, pagos pli da mono, sendepende de la maniero kiel ili uzos la maŝinon unuamomente. Ĉi tiu mono taŭgos por fronti du elspezojn: tiun de la maŝino kaj tiun de la jara luo al provizisto de la reta linio kaj de la fizika spaco kie lokigi la maŝinon.

**La bankokonto estas: 3025 0006 21 1433230004 (Caja de Ingenieros)**

[sd@sindominio.net](mailto:sd@sindominio.net)

[www.sindominio.net](https://sindominio.net)

