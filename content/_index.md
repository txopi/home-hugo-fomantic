---
title: "Inicio"
date: 2019-07-11T20:26:23+02:00
draft: false

# OTROS CONTENIDOS EN PORTADA
# El contenido de las tarjetas de servicios y el menú inferior de portada 
# está en /data/ en los ficheros menufooter.yaml y tarjetas.yaml

---
Sindominio pretende sumergirse en todo ese multiverso que se autoorganiza y se mueve por la red. Se trata de hacer visibles y potenciar las realidades antagonistas que ahora se encuentran dispersas o fuera de la red. Queremos aportar a estos espacios de cooperación y de comunicación –y también de conflictos y luchas– donde existen proyectos autogestionados de dimensiones extraordinarias, como el que dio lugar al sistema operativo GNU/Linux, el mayor exponente –junto a la propia red Internet– de construcción colectiva a través del ciberespacio. Por eso necesitamos una máquina GNU/Linux donde probar con herramientas libres y colaborativas, investigar juntas e intercambiar conocimientos con otras comunidades virtuales en el estratégico campo del software. Aquí baila Sindominio.  
