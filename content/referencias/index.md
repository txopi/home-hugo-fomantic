---
Title: Referencias
---
En nuestro transcurrir nos hemos cruzado con colectivos que han aportado o aportan algo a sindominio, desde una visión política a recursos personales y materiales. Os dejamos enlaces a algunos de ellos. 

## Telemáticas

####  Isole nella Rete
{{< figure src="img/IsoleNellaRete.jpg" alt="IsoleNellarete" >}}

La histórica ECN (European Counter Network) fue pionera del activismo en la red, primero como red de BBSs y luego en Internet, y propuso a todo el movimiento antagonista un modo distinto de entender la contrainformación, alejado de los prejuicios tecnófobos que padecimos en otras partes. Ha sido un punto de referencia fundamental para proyectos como sinDominio, que sería difícil de imaginar sin la ECN. A la ECN se debe el extraordinario desarrollo que ha tenido la telemática antagonista en Italia, y el papel central que siempre ha realizado esta en el movimento en general y en los centros sociales en particular.

#### Samizdat
[![Samizdat](./referencias/img/samizdat.jpg)](http://www.samizdat.net/)

Proyecto francés con larga experiencia —algunos de sus fundadores pertenecieron a la ECN (European Counter Network), que trata de alejarse de los caminos trillados de la propaganda y comparte muchos aspectos con SinDominio.

#### Tactical Media Crew
[![Tactical Media Crew](./referencias/img/tmcrew.jpg)](https://www.tmcrew.org/)

Un proyecto que nace principalmente de la necesidad de dejar espacio a diferentes realidades sociales básicas, con una atención específica para todo aquello que normalmente es definido como Movimiento Antagonista en Internet.

#### Nadir
[![Nadir](./referencias/img/nadirlogo.gif)](https://www.nadir.org/nadir/)

Veterano proyecto alemán.

#### Autistici
[![Autistici](./referencias/img/autistici.jpg)](https://www.autistici.org/)

A/I nació hace más de 10 años cuando individuos y colectivos ocupad\*s en cuestiones de tecnología, privacidad, derechos digitales y activismo político se reunieron en Italia. Nuestra idea básica es proveer una amplia variedad de herramientas de comunicación libres, impulsando a las personas a elegir aquellas opciones libres en lugar de las comerciales. Queremos informar y formar sobre la necesidad de proteger nuestra propia privacidad con el fin de evitar el saqueo que gobiernos y corporaciones llevan a cabo de forma indiscriminada sobre nuestros datos y personalidades.

A/I ofrece una amplia gama de servicios gratuitos, libres y respetuosos de la privacidad


#### Riseup

[![Riseup](./referencias/img/riseup.jpg)](https://riseup.net/)

Colectivo de voluntarixs que ofrece cuentas de correo electrónico seguras, lista de correo, VPN, chat entre otros servicios. La organización fue lanzada por hacktivistas en Seattle durante 1999. Su misión es apoyar el cambio social liberador a través de la lucha contra el control social y la vigilancia masiva a través de la distribución de herramientas seguras.

 
## Comunicación

#### El Salto
[![El Salto](./referencias/img/el_salto.png)](https://www.elsaltodiario.com/)

Periódico mensual y medio diario digital de actualidad crítica en el Estado español. Continuación del periódico Diagonal (2005-2016). Proyecto formado desde 2017 por cerca de 200 personas y cerca de 7.000 socios/as que apuesta por un periodismo radicalmente diferente: sin financiación de empresas del Ibex35, democrático, descentralizado y de propiedad colectiva.

#### La Directa
[![La Directa](./referencias/img/ladirecta.jpg)](https://directa.cat)

mitjà de comunicació en català d’actualitat, investigació, debat i anàlisi. Amb vocació de contribuir a la transformació social, vol exercir la funció de denunciar els abusos i les injustícies i potenciar les alternatives. Gràcies a l’existència d’una xarxa de nuclis territorials i col·laboradores que s’ha anat teixint des de 2006, el mitjà arriba a unes 3.000 subscriptores que donen suport al projecte.
 
#### A-Infos
[![A-Infos](./referencias/img/ainfos2.jpg)](http://www.ainfos.ca/ca/)

Agencia multilingüe de noticias de, por y para anarquistas.

## Software libre

#### Fundación para el Software Libre
[![GNU](./referencias/img/gnu-head-sm.jpg)](https://www.gnu.org/)

Fundación para promover el software libre, y en especial el proyecto GNU: un sistema operativo tipo UNIX totalmente libre. La FSF fue pionera en el origen y definición del movimiento del software libre, y a ella se debe la licencia GPL, que es la plasmación del concepto copyleft y auténtico bastión jurídico del software libre. Su papel sigue siendo clave y muy respetado.

#### Debian
[![Debian](./referencias/img/debian.jpg)](https://www.debian.org/)

El Proyecto Debian tiene como objetivo recopilar, mantener y distribuir un sistema operativo libre completo. Es la única distribución de GNU/Linux basada exclusivamente en software libre y es la única de carácter no comercial. Debian se mantiene y desarrolla de manera distribuida mediante la cooperación desinteresada de más de 1000 hackers de todo el mundo y dispone de una comunidad de decenas de miles de usuarios coordinados a través de cerca de cien listas de correo públicas. Los dos servidores de sinDominio emplean Debian GNU/Linux como sistema operativo.

 
## Ciberderechos-Hacktivismo

#### Electronic Frontier Foundation
[![EFF](./referencias/img/eff.png)](https://www.eff.org/)

Fue la primera organización específicamente constituida para defender los derechos digitales. Con el tiempo se ha convertido en la principal referencia en su ámbito y en un auténtico lobby en defensa de los ciberderechos en Estados Unidos.

#### Chaos Computer Club
[![CCC](./referencias/img/ccc.png)](https://www.ccc.de/)

El histórico club de hackers alemán ha sido un ejemplo para generaciones de hacktivistas, especialmente por su defensa del flujo de la información sin censuras.

#### Calafou
[![Calafou](./referencias/img/calafou.png)](https://calafou.org/)

Colonia EcoIndustrial Postcapitalista

#### Xnet
[![Xnet](./referencias/img/xnet.png)](https://xnet-x.net/)

Plataforma formada por activistas que trabajan en diferentes campos relacionados con la democracia en red y la defensa de la neutralidad de la red. Sus líneas de acción giran en torno a cinco ejes: la libre circulación de la cultura, la neutralidad de la Red, la tecnopolítica, la defensa del periodismo ciudadano y la lucha legal contra la corrupción. También realizan un trabajo de presión política, a nivel estatal e internacional, a través de propuestas legislativas y campañas virales


#### Hacklabs
[![Haclabs](./referencias/img/hl.png)](https://www.hackmeeting.org/)

A su sombra hemos crecido. Se reunen en hackmeetings.
