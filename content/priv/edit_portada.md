---
title: "Edición de la_portada web"
date: 2019-10-18T01:04:54+02:00
draft: false
---

La portada de sd es una html estático generado por hugo.
Un sitio de hugo es un árbol de directorios con ficheros de contenido escritos con sintáxis markdown (no hay base de datos), imágenes, css, js, plantillas (escritas en go), configuración...  

El repositorio git https://git.sindominio.net/sindominio/portada/ es el sitio hugo de sindominio para  la portada.
  
#### https://sindominio.net/portada/

  
</br>
#### CONTENIDO

Así, en nuestro sitio hugo (repo) tenemos una carpeta `content/` donde crearemos nuestros ficheros .md, que contienen nuestros textos para el contenido.  
Encabezando los ficheros tenemos el 'frontmatter', un bloque donde podemos definir algunos metadatos del contenido. Si bien no es necesario que exista ningún campo, conviene definir como mínimo el título. Si nuestro sitio web va a mostrar fechas deberemos definir también la fecha.  
Por comodidad para crear un nuevo contenido basta con ejecutar:
```
hugo new post/hola_mundo.md
```
Hugo creará el fichero y escribirá una cabecera básica con el título, la fecha, y el campo 'draft' como true; creará un borrador. Tendremos que cambiar el campo a false para que se incorporé resultado estático.  
Dentro de content/ podemos organizar el contenido en carpetas denominadas como nos guste. Formarán parte de la url en la que se accede a los contenidos de la carpeta.  
Por ejemplo un fichero `content/priv/hola_marte.md` se accederá en https://sindominio.net/priv/hola_marte.  
Estas carpetas serán nuestras secciones. Si en la raíz de cada sección creamos un fichero index.md dicha sección tendrá su página propia, desde donde podremos por ejemplo enlazar el resto de páginas.



</br>
#### DEFINIR EL TEMA

Antes de generar el primer resultado estático es necesario que exista un tema en /themes/, y que esté definido en el fichero config.toml en la raíz del sitio.
En nuestro caso tenemos:

```
theme = "minimal-bootstrap-hugo-theme"
```
y el tema descardado en `/themes/minimal-bootstrap-hugo-theme`.


</br>
#### GENERAR EL CONTENIDO ESTÁTICO

Pasamos a generar el html a partir de nuestro árbol de directorios:
```
  hugo -s ruta_al_árbol -d ~/static_ouput_dir -b baseURL'
```

`-s` source, nuestro sitio hugo.  
`-d` destination, salida html más assets (img, css, js, ...).  
`-b` baseURL, el dominio (y en su saco carpeta) que se usará para escribir las direcciones de los assets.  

Por ejemplo:
```
hugo -s ~/hugo/mi_sitio -d ~/public_html -b https://midominio.net/mi_sitio/
```



</br>
#### MODIFICAR EL TEMA - CSS y PLANTILLAS

La carpeta `/static/` alberga las imágenes, estilos y demás ficheros que se servirán en nuestra web. Podemos crear carpetas para ordenar estos ficheros, como `css/` o `img/`. El html resultante, por medio de las plantillas, podrá incluir estos recursos como https://sindominio/css/style.css y http://sindominio.net/img/myimage.png.

Así, en la carpeta css podemos sobreescribir el css del tema.

Las plantillas del tema las sobreescribimos en la carpeta `layouts/`. Allí replicamos la estructura de carpetas necesaria para copiar la plantilla a modificar y modificarla.

Por ejemplo, tenemos en layouts/partial/head.html sobreescrito el mismo fichero del tema con el siguiente añadido:
```
{{ range .Site.Params.custom_css -}}
    <link rel="stylesheet" href="{{ . | absURL }}">
{{- end }}
```
`range` en go es equivalente a 'Para cada elemento', un bucle for en otros lenguajes.

`.Stite.Params.customs_css` es un campo que hemos definido en nuestro `config.toml` con `custom_css = ["css/style.css"]`.

El punto `.`, en href, hace mención al elemento de la lista que `range` itera, en nuestro caso con sólo un elemento: `css/style.css`.

Por último `| absURL` es el valor en caso de que no haya elementos en la variable. Sería nuestra url a pelo, lo que no tiene mucha utilidad seguramente.

Para crear una página principal hemos creado `/layout/index.html` y `content/_index.md`, plantilla y contenido respectivamente. En el segundo fichero hemos definido varios campos (3 listas) en el frontmatter que luego hemos usado luego en la plantilla.

Cada vez que modificamos algo, podríamos ejecutar el comando hugo mencionado anteriormente y regenerar el html para ver cómo queda. Pero mucho más cómodo es usar:
```
hugo -D server
```
Esto genera el resultado estático y lo sirve http://localhost:1313 y observa modificaciones en el sitio. Al guardar/modificar cualquier fichero regenera automáticamente el estático y actualiza nuestra página en el navegador sin que sea necesario refrescar.



</br>
#### GIT

Dado que nuestro sitio web está en un repositorio git lo primero que haremos para empezar a trabajar será clonarlo a nuestra máquina local.
```
git clone https://git.sindominio.net/sindominio/portada.git
```
Entramos en la carpeta clonada y podemos arrancar directamente nuestro hugo server local con el comando mencionado.

Tocamos el tema, añadimos nuevas páginas, las enlazamos en otras, creamos una nueva sección de contenido,  añadimos nuestro media ...

Y cuando creamos que hemos avanzado lo suficiente como para actualizar el sitio usamos comandos git para subir el contenido.
```
git add --all
git commit -S -m 'Comentario a la actualización'
git push
```

> **Atención:** `-S` firma los commit, las subidas que hacemos al repo. Para hacerlo tenemos que tener una clave gpg en nuestra máquina y haber subido nuestra parte pública a nuestro perfil en gitea. Si no tienes, es tiempo de tenerla. Puedes quitar '-S' en cualquier caso.



</br>
#### DESPLIEGUE AUTOMÁTICO

Cada actualización (push) que hacemos al repo, lanza un webhook, una llamada al servidor donde ser sirve la web con un token de autenticación que el otro extremo puede verificar. Allí, tras la verficación, se lanza un script que básicamente lo que hace es clonar el repo y ejecutar hugo para generar el estático. Finalmente mueve el estático a la carpeta que publica el servidor web.  
Así, para commit en git se regenera la web.


