---
title: "Participación"
---
Actualmente existen dos formas de participación en Sindominio (SD).

#### Sindominantes

Por una parte las Sindominantes, que son las personas que forman parte del colectivo de SD. Participan en la asamblea general, en presenciales y en grupos de trabajo diversos, hacen aportaciones económicas anuales y utilizan los recursos tecnológicos de los que ellas mismas se dotan.

El proceso de entrada a SD como Sindominante tiene un protocolo de amadrinamiento. Alguien que ya es Sindominante avala a la nueva persona que solicita serlo, confirma que se ha leído los textos donde se explica qué es y cómo funciona SD y que confía en esa persona como nueva integrante de la Asamblea. 
A pesar de ser un colectivo telemático, creemos que SD logra tejer así la red de confianza gracias a la cual se mantienen los grupos asamblearios.

La adscripción a Sindominio, al menos actualmente, es a nivel de personas, no de colectivos. Por tanto, quienes son Sindominantes y participan en la asamblea de Sindominio son personas. Eso no quita para que muchas, por no decir todas las personas, participen en colectivos sociales.

#### Amigas

La segunda forma de pertenencia es como Amiga. Esto es mucho más rápido. 
Cualquier Sindominante puede invitar a amigas para que utilicen (se entiende que de forma responsable) los recursos tecnológicos que ofrece SD como son e-mail, blog, chat, etc. Es decir, que si lo que quieres es tener una cuenta de correo electrónico (y cerrar la tuya de google o yahoo) y usar con esa cuenta herramientas como Matrix o Jabber (en lugar de Whatsapp, Telegram o Skype), sólo necesitas que te invite cualquier Sindominante. Después te creas la cuenta tú misma en un periquete y ya está.
Además, puedes hacer una aportación económica a SD de acuerdo a tus condiciones materiales. Una aportación orientativa oscilaría entre los 10 y 30 euros anuales.
¿Conoces a alguna Sindominante? ¿Quizás sí y no lo sabes? Si es así, coméntalo con esa persona y hazte amiga con su invitación.

#### Colectivos

Los colectivos pueden ser Amigas de Sindominio, acceder a todas las herramientas y servicios que utilizamos, así como contribuir económicamente. 
Para colectivos, una aportación orientativa oscilaría entre 30 y 60 euros anuales. Además, en cualquier momento los colectivos pueden hacer donaciones a SD.   
  
  
---
  
  
Las aportaciones económicas no están burocratizadas ni son fijas, pues responden a momentos personales y condiciones materiales. Por eso, no hay una cuota fija anual establecida. Las aportaciones recomendadas para poder seguir adelante se hablan en la asamblea. La Comisión-Grupo de Economía de Sindominio ordena y actualiza periódicamente las cuentas.
