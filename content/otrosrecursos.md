---
Title: "Recursos digitales"
---
#### OTROS SERVIDORES AUTÓNOMOS

Autistici:https://www.autistici.org/

Riseup: https://riseup.net/

Framasoft: https://framasoft.org/en/

Systemli: https://www.systemli.org/

Guifi.net: http://guifi.net/ 

List Jabber: https://list.jabber.at/


#### COOPERATIVAS QUE OFRECEN SERVICIOS 

DABNE: https://www.dabne.net/

ONDULA: https://ondula.org/

CODIGO SUR: https://codigosur.org/

NODO50: https://info.nodo50.org/


#### INVESTIGACCIÓN

Infra.red

Xnet: https://xnet-x.net/

#### ADMINISTRADORES DE CONTRASEÑAS

KeePassXC: https://keepassxc.org/

1Password: https://1password.com/

Passpack: https://www.passpack.com/

Dashlane: https://www.dashlane.com/

#### BUSCADORES

DUCK DUCK GO! https://duckduckgo.com

GIBIRU: https://gibiru.com/

#### NAVEGADORES

Tor Browser: https://www.torproject.org/download/

Mozilla Firefox: https://www.mozilla.org/en-US/firefox/new/


#### REDES SOCIALES

Fediverso: https://fediverse.party/

Mobilizon: https://mobilizon.org/en/

Pleroma: https://pleroma.social/

Mastodon: https://joinmastodon.org/

Hubzilla: https://start.hubzilla.org 

Diaspora: https://joindiaspora.com/

GNU Social: https://gnu.io/ 

#### MAIL

Gestor de mail Mozilla Thunderbird: https://www.thunderbird.net/es-ES/

Todos los servidores autónomos y cooperativas tecnologicas ofrecen mails y listas de mail.


#### CHATS- SERVICIOS DE MENSAJERIA INSTANTÁNEA

IRC: https://irssi.org/download/

Element: https://element.io/

Tox: https://tox.chat/

Jami: https://jami.net/

Wire: https://wirechat.com/

Signal: https://www.signal.org/

Slack:https://slack.com/intl/es-es/

Rocket: https://rocket.chat/

Discord: https://discord.com/

Nextcloud Talk: https://nextcloud.com/talk/

#### DESCARGA DE APPS- PROGRAMAS

F-droid: https://f-droid.org/

#### HERRAMIENTAS COLABORATIVAS 

Nube: https://nextcloud.com/

Pads: 
https://cryptpad.fr/
https://pad.codigosur.org
https://pad.riseup.net

Pads con markdown: 
https://demo.codimd.org/

Hojas de cálculo:
https://ethercalc.net/

Encuestas y formularios:

https://www.limesurvey.org/en

https://framasoft.org/en/

https://cryptpad.fr/

Pizarra: https://cryptpad.fr/whiteboard/

Organización del trabajo: Kanban (cryptpad); https://cryptpad.fr/kanban/ 

Enviar archivos pesados: FTPs (file transfer protocol) con servidora segura

Share riseup: https://share.riseup.net/

Upload Disroot: https://cloud.disroot.org/login

Catdrop: https://catdrop.drycat.fr/ 

https://www.systemli.org/

https://list.jabber.at/

#### COOPERATIVAS 

https://codigosur.org

https://ondula.org/

https://info.nodo50.org/

Drop.infini: https://drop.infini.fr/


#### TEXTOS, PRESENTACIONES, HOJAS DE CÁLCULO, CONVERSOR A PDF

OpenOffice: https://www.openoffice.org/ 

LibreOffice: https://www.libreoffice.org/ 


#### VIDEO Y MULTIMEDIA

El repositorio multimedia por excelencia: https://archive.org

El youtube libre y P2P: https://peertube.cpy.re/

Para emisiones por streaming: 

http://giss.tv/interface/interface_info.php

https://github.com/Streampunk/naudiodon

Para editar vídeo: OPENSHOT https://www.openshot.org/ 

Para visualizar y editar video y audio: VLC https://www.videolan.org/vlc/

Para streaming: BUTT, MIXX y OBS.


#### IMÁGENES

Repositorios de imágenes con licencias abiertas: 

Wkimedia Commons: https://commons.wikimedia.org/wiki/Main_Page

Public Domain Pictures: https://publicdomainpictures.net/

Unplash: https://unsplash.com/

Wunderstock: https://wunderstock.com/

Foodiesfeed: https://www.foodiesfeed.com/

OGraphy: https://gratisography.com/

Isorepublic: https://isorepublic.com/

Imágenes, vetores e iconos  (todas las licencias): https://www.svgrepo.com/

Negative Space: https://negativespace.co/

Pexels: https://www.pexels.com/es-es/ 

Photopin: http://photopin.com/

Pixabay: https://pixabay.com/

Stocksnap: https://stocksnap.io/

Para editar imagen: GIMP https://www.gimp.org/

Para maquetar: SCRIBUS https://www.scribus.net/

Para ASCII ART: http://www.patorjk.com/

Para gifs: https://gifmaker.me/ 

#### MÚSICA Y AUDIO

https://freemusicarchive.org/

https://www.jamendo.com/

https://freesound.org/

Para editar audio: 

AUDACITY: https://www.audacityteam.org/
ARDOUR: https://ardour.org 

#### MAPAS Y TRADUCTORES

Mapas: https://www.openstreetmap.org/#map=4/-15.13/-53.19

Traductores: https://www.deepl.com/translator


#### MÁQUINAS VIRTUALES (VIRTUAL MACHINES- VM)

Infraestructura en rack: https://guifi.net
Tachanka: https://tachanka.org/
Tetaneutral: https://tetaneutral.net/

#### AUDIOLLAMADAS

Mumble: https://www.mumble.info/


#### VIDEOLLAMADAS

Jitsi (web, móvil y desktop): https://meet.jit.si/

https://vc.autistici.org

Videollámame: Para personas mayores sin conocimientos tecnológicos https://videollama.me

Jitsimeter: buscador de instancias Jitsi, clasificando por velocidad y características: https://ladatano.partidopirata.com.ar/jitsimeter/

BigBlueButtom (permite subgrupos, educación): https://bigbluebutton.org/

https://edu.cisti.org (hacklab Torino)

Discord: https://discord.com/

P2p Chat: https://p2p.chat/

Jami: https://jami.net/


#### BORRAR METADATOS

Mat2: https://metadata.systemli.org

#### COMPARATIVAS DE PLATAFORMAS EN WIKIPEDIA

https://en.wikipedia.org/wiki/Comparison_of_cross-platform_instant_messaging_clients

https://en.wikipedia.org/wiki/Comparison_of_VoIP_software

https://en.wikipedia.org/wiki/Comparison_of_web_conferencing_software

#### OTROS ENLACES DE INTERÉS 

https://liberaturadio.org/recursos-y-plataformas-libres/

http://www.underworld.fr/

https://snopyta.org/

https://dismail.de/

https://nixnet.services/

https://pixie.town/ Vectores e iconos (todas las licencias): https://www.svgrepo.com/

https://wp.sindominio.net/blog/infraestructuras-solidarias-para-acciones-solidarias/
          
