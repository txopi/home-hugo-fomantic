---
title: "Quiénes somos"
date: 2019-10-01T10:53:16+02:00
draft: false
---

* [¿Quiénes somos?](#quiénes-somos)
* [¿Por qué existe sindominio.net?](#por-qué-existe-sindominio-net)
* [¿Cuáles son nuestros objetivos?](#cuáles-son-nuestros-objetivos)
* [¿Cómo puedo entrar a participar? ¿Qué implica hacerlo?](#cómo-puedo-entrar-a-participar-qué-implica-hacerlo)
* [Pero entonces ¿SinDominio es algo exclusivamente telemático?](#pero-entonces-sindominio-es-algo-exclusivamente-telemático)
* [¿Cómo se cubren los gastos que hay en Sindominio?](#cómo-se-cubren-los-gastos-que-hay-en-sindominio)
* [¿Qué tiene que ver Sindominio con el software libre?](#qué-tiene-que-ver-sindominio-con-el-software-libre)
* [¿Qué tienen que ver los hacklabs con Sindominio?](#qué-tienen-que-ver-los-hacklabs-con-sindominio)

## ¿Quiénes somos?

Sindominio es un servidor de internet autónomo y autogestionado por una comunidad de seres que apuestan por formas de funcionamiento digital horizontales y seguras. Sindominio nació en 1999, se organiza de manera asamblearia y hoy sigue siendo un espacio para el aprendizaje mutuo y el empoderamiento tecnológico. Sindominio provee a activistas de herramientas y servicios en internet como correo electrónico, listas de correo, mensajería instantánea y páginas web, entre otras.

## ¿Por qué existe sindominio.net?

A finales del siglo XX internet se nos mostraba como un un gran medio de comunicación: permitía una comunicación segura, rápida y versátil, intercambiar cualquier tipo de mensaje, texto, imágenes, audio, video, … en principio. Todas estas características hacían de la red un lugar que los colectivos alternativos debían habitar. Sin embargo nuestro acceso al espacio pasaba por el filtro de las empresas que nos proveían de conexión, espacios web, correos ...

Frente a esto, gente perteneciente a movimientos de Madrid y Barcelona consideró necesario la creación de un espacio libre en Internet, donde no hubiese empresarios ni clientes, donde las posibilidades de utilización de Internet como medio no estuviesen limitadas a lo que pagas, dónde toda la gente participase en la toma de decisiones… Para ello era necesario situar una computadora en Internet las 24 horas del día, organizarla, publicitarla… Esa fue la idea original de Sindominio, y eso es lo que sigue siendo.

De aquella potencial horizontalidad inicial de Internet queda poco. Ya ni siquierea somos clientes como en esos primeros años, sino más bien comportamientos datificables y por tanto monetarizables. Seguimos fuera de cualquier toma de decisión, de cualquier posibilidad de participar y decidir sobre el manejo de nuestra propia información y de la que nos llega y nos modela.

Para romper esa lógica estamos aquí, porque vivir la autogestión en lo virtual sigue siendo nuestro reto.

## ¿Cuáles son nuestros objetivos?

En SinDominio tratamos de construir un espacio en Internet que se administre horizontalmente y que sea de utilidad para la transformación social. Este es un objetivo amplio, tal vez ambiguo, y por ello se redefine cada día en nuestra asamblea virtual (una lista de distribución) y en nuestros grupos de trabajo (otras listas de distribución).
 
## ¿Cómo puedo entrar a participar? ¿Qué implica hacerlo?

Tenemos dos modelos de participación en sindominio. Puedes ser nuestra amiga, o sindominante.
La amigas pueden utilizar nuestros servicios y cooperan económicamente en el sostenimiento del proyecto. Para ser amiga deberás recibir una invitación de un sindominante.

Ser sindominante implica participar de la asamblea y de la construcción colectiva del proyecto. Nuestro colectivo se basa un anillo de confianza, por lo que para ser sindominante tendrás que conocer a alguien que ya lo sea. Puedes ponerte en contacto con nosotr@s a través de sd@sindominio.net y te contamos más.

Más información en [Participación](../participacion).

## Pero entonces ¿SinDominio es algo exclusivamente telemático?

SinDominio se concibió a partir de encuentros presenciales de contrainformación y hoy día, seguimos encontrándonos en espacios físicos, dónde volvemos a tratar temas que se hablan en la asamblea virtual. Aunque a estos encuentros se llega con un orden del día, tal vez lo más importante de estos no son las decisiones que se toman (debido a que estas deberán ratificarse en la lista asamblea), sino los lazos personales que se crean y las ganas que se generan de hacer más y más cosas.
 
## ¿Cómo se cubren los gastos que hay en Sindominio?

Mediante aportaciones de quienes lo formamos. No hay cuotas, ni precios ni nada que se le parezca, pero puesto que SD comporta gastos, se espera que quienes participamos en el proyecto nos responsabilicemos también de su financiamiento en la medida que podamos. A modo orientativo, se recomienda una aportación de unos 5-20 euros/año, pero cada cual aporta lo que quiere/puede. 

Nuestro número de cuenta es **3025 0006 21 1433230004 (Caja de ingenieros)**, por si tú también quieres aportar algo 😉

Más información en [Apoyo](../apoyo).
 
 
## ¿Qué tiene que ver Sindominio con el software libre?

El software libre es software que utiliza una fórmula del copyright que muchos llaman copyleft, que entre otras cosas permite la libertad de copia y modificación del código de ese software, creando por tanto, una propiedad colectiva y universal alrededor de ese software.

Sindominio es un proyecto político de transformación social y también un proyecto telemático es por ello coherente con la realidad de sindominio participar en la comunidad del software libre e incluso tratar de llevar la filosofía del copyleft a otras esferas de la información, dónde esta licencia tiene menos tradición, pero no menos potencial, como pueden ser los materiales escritos, la música, las imágenes, el vídeo, ó incluso otras áreas que no son información.
 
## ¿Qué tienen que ver los hacklabs con Sindominio?

En los hacklabs se trata de experimentar con los usos sociales de la tecnología y esto es algo bastante afín a las motivaciones de los sindominiantes es por ello que no es extraño encontrar a muchos sindominiantes en los hacklabs y que las páginas y listas de hacklabs y el propio hackmeeting se encuentre en Sindominio, pero no es algo exclusivo de sindominiantes, sino que son proyectos autónomos aunque tengan relaciones de afinidad con Sindominio.
